import React from 'react';
import { Platform } from 'react-native';
import { createStackNavigator,} from 'react-navigation';
import {createMaterialBottomTabNavigator} from 'react-navigation-material-bottom-tabs'

import TabBarIcon from '../components/TabBarIcon';
import Services from '../screens/Dashboard/BottomNavBar/Services/services'
import Addcar from '../screens/Dashboard/BottomNavBar/Addcar/addcar'
import Overview from '../screens/Dashboard/BottomNavBar/Overview/overview';
import Profile from '../screens/Dashboard/BottomNavBar/profile/profile'
import DashboardScreen from '../screens/Dashboard/BottomNavBar/Dashboard'
import VinDecoder from '../screens/Dashboard/BottomNavBar/Decoder/decoder'
import StolenCar from '../screens/Dashboard/BottomNavBar/StolenCar/StolenCar';
import Documents from '../screens/Dashboard/BottomNavBar/Document/document'
import CarSchedules from '../screens/Dashboard/BottomNavBar/ServiceTracking/Tracking'



const homeStack = createStackNavigator({
    home: DashboardScreen,
    decoder: VinDecoder,
    stolenCar: StolenCar,
    document: Documents,
    schedules:CarSchedules
  },{
    initialRouteName:'home',
    defaultNavigationOptions:{
     headerStyle:{ backgroundColor:"#101112",},
     headerTintColor:"white"   
    }
  } 
);
  

  homeStack.navigationOptions = {
    tabBarLabel: 'Home',
    tabBarIcon: ({ focused }) => (
      <TabBarIcon
        focused={focused}
        height={18}
        width={21}
        source={require("../assets/images/home.png")}
       
      />
    ),
  };
  


const scheduleServiceStack = createStackNavigator({
  servicesScreen: Services,
});

scheduleServiceStack.navigationOptions = {
  tabBarLabel: 'Services',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
    focused={focused}
    height={20}
    width={30}
    source={require("../assets/images/service.png")}
   
  />
  ),
};

const AddCarStack = createStackNavigator({
  addCarScreen: Addcar,
});

AddCarStack.navigationOptions = {
  tabBarLabel: 'Add Car',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
    focused={focused}
    height={19}
    width={25}
    source={require("../assets/images/sports-car.png")}
   
  />
  ),
};

const OverviewStack = createStackNavigator({
     overviewScreen: Overview,
});

OverviewStack.navigationOptions = {
  tabBarLabel: 'Overview',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
    height={20}
    width={20}
    focused={focused}
    source={require("../assets/images/clock.png")}   
  />
  ),
};


const ProfileStack = createStackNavigator({
    ProfileScreen: Profile,
  });
  
  ProfileStack.navigationOptions = {
    tabBarLabel: 'Profile',
    tabBarIcon: ({ focused }) => (
      <TabBarIcon
      focused={focused}
      height={20}
      width={20}
      source={require("../assets/images/user.png")}
     
    />
    ),
  };
  


const main = createMaterialBottomTabNavigator({
  homeStack,
  scheduleServiceStack,
  AddCarStack,
  ProfileStack,
},

{  
    initialRouteName: "ProfileStack",  
    activeColor: '#ffffff',  
    inactiveColor: '',
    barStyle: { backgroundColor: "#101112",
              
               },
    
    
  },  



);

export default main