import React from 'react';
import { createStackNavigator} from 'react-navigation';
import OnBoardScreen from '../screens/OnbaordingScreens/OnboardScreen'
import LoginScreen from '../screens/Login/LoginScreen';




const SignedOutNavigator = createStackNavigator({
	Onboarding: {screen: OnBoardScreen},
	Login: {screen: LoginScreen},

}, 

{
	initialRouteName: "Onboarding",
	mode: "modal",
	headerMode: "none",	
}
);

export default SignedOutNavigator