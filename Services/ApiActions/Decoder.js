/**
 * @export decodeSuccess
 * @export decodeError
 * @export Requestdecoder
 * @export decodeVin
 * 
 */


import {DECODE_REQUEST,DECODER_SUCCESS,DECODER_ERROR} from '../../constants/reduxConstants';
import {Alert} from 'react-native'
/**
 * @function  decodeSuccess
 * @param {string} json
 * @return JSON data
 */

export const decodeSuccess =json => ({
     type:DECODER_SUCCESS,
     payload:json,
});



/**
 * @function decodeError 
 * @param {string} error
 * @return string data
 */

export const decodeError = error => ({
  type: DECODER_ERROR,
  payload: error,
});



export const Requestdecoder = ()=>({type: DECODE_REQUEST});
 

  /**
   * @function  decodeVin 
   * @param {string} vin
   * @return {JSON} response
   * Retrieves VIN teaser details from API 
   */




    export const decodeVin = (vin) =>{
          
       
      return async dispatch =>{
        try{

          dispatch(Requestdecoder());
          let response = await fetch("https://api.carfacts.ng/api/vincheck/" + vin
    
        );
         let json = await response.json();    
          

         if(json.data){
          dispatch(decodeSuccess(json.data));
        
        
           
         }
         else{
           Alert.alert('','Result not found')
         }
      

        
        }catch(error){
           dispatch(decodeError(error));
           Alert.alert('','Network Error')
        }
      }}
