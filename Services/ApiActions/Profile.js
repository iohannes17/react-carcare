import {CREATE_PROFILE_ERROR,CREATE_PROFILE_REQUEST,CREATE_PROFILE_SUCCESS} from '../../constants/reduxConstants';
import axios from 'axios';
import {Alert,AsyncStorage} from 'react-native'


export const ProfileSuccess =json => ({
     type:CREATE_PROFILE_SUCCESS,
     payload:json,
});

export const ProfileError = error => ({
  type: CREATE_PROFILE_ERROR,
  payload: error,
});

export const ProfileRequest = ()=>(
    {
        type: CREATE_PROFILE_REQUEST
    }
);





export const CreateProfileApp = (userData) =>{
    
    return async dispatch =>{

    try{
           
                  dispatch(ProfileRequest());
                 

                  axios({
                    method: 'post',
                    url: 'https://dna4wheels-staging.herokuapp.com/auth/mobile/login',
                    data: userData
                  })  .then(response=> {
                       
                         AsyncStorage.setItem('userToken',response.data.data.token)
                         dispatch(ProfileSuccess('Updated Successfully'));
                         Alert.alert('','Profile Updated Successfully')
                  }).catch(error =>{
                    Alert.alert('','update error')
                     dispatch(ProfileError(false))
                    })
              
    }catch(error){
          dispatch(ProfileError(error));
          Alert.alert('','Update error')
     }};

     };
    
    



  






























export const pushSuccess =json => ({
  type:PUSH_SUCCESS,
  payload:json,
});

export const pushError = error => ({
type: PUSH_ERROR,
payload: error,
});


/**
 * Send device token to database
 */


export const registerForNotification = (name) =>{
    
  return async dispatch =>{
    try{

     const value = await AsyncStorage.getItem('deviceIDToken');

     const details = {
       token: value,
       userID: name
     }
     
      
   let response =  fetch(PUSH_ENDPOINT, {
                method: 'POST',
                headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                },
              body: JSON.stringify(details),
	});
  
       let json = await response.json();    
      
    }catch(error){
       dispatch(fetchEventHistoryError(error));
       ToastAndroid.showWithGravity(
         'Network Error',
         ToastAndroid.SHORT,
         ToastAndroid.BOTTOM,
       );
    }
  
  };
 } ;



