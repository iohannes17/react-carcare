import {GET_CAR_ERROR,GET_CAR_SUCCESS} from '../../constants/reduxConstants';
import axios from 'axios';
import {Alert,AsyncStorage} from 'react-native'



export const getCarSuccess =json => ({
     type:GET_CAR_SUCCESS,
     payload:json,
});

export const getCarError = error => ({
  type:GET_CAR_ERROR,
  payload: error,
});


export const getUserCars = () =>{
    
    return async dispatch =>{

    try{
           
                   const token = await AsyncStorage.getItem('userToken')

                  axios({
                    method: 'get',
                    url: 'https://dna4wheels-staging.herokuapp.com/car/me',
                    headers:{
                      'x-access-token':token,
                  }
                  }) .then(response=> {
                        
                    let arr =  response.data.data;
                    let newArr=[];
                    
                    for (var i = 0; i < arr.length; i++){
                      var obj = arr[i];
                      var b =
                        {
                          "brand":obj.carDetail.carBrand + ' '+ obj.carDetail.model,
                           "id": obj.carDetail.id,
                         }
                      newArr.push(b)	
                    }
                
                 
                    dispatch(getCarSuccess(newArr));
                       
                  }).catch(error =>{
                     console.log(error)
                     dispatch(getCarError(error))
                    })
              
    }catch(error){
        dispatch(getCarError(error))
     }};

     };
    
    



  






























export const pushSuccess =json => ({
  type:PUSH_SUCCESS,
  payload:json,
});

export const pushError = error => ({
type: PUSH_ERROR,
payload: error,
});


/**
 * Send device token to database
 */


export const registerForNotification = (name) =>{
    
  return async dispatch =>{
    try{

     const value = await AsyncStorage.getItem('deviceIDToken');

     const details = {
       token: value,
       userID: name
     }
     
      
   let response =  fetch(PUSH_ENDPOINT, {
                method: 'POST',
                headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                },
              body: JSON.stringify(details),
	});
  
       let json = await response.json();    
      
    }catch(error){
       dispatch(fetchEventHistoryError(error));
       ToastAndroid.showWithGravity(
         'Network Error',
         ToastAndroid.SHORT,
         ToastAndroid.BOTTOM,
       );
    }
  
  };
 } ;



