import {LOGIN_ERROR,LOGIN_REQUEST,LOGIN_SUCCESS,PUSH_ERROR,PUSH_REQUEST,PUSH_SUCCESS} from '../../constants/reduxConstants';
import {ToastAndroid,AsyncStorage} from 'react-native'



export const loginSuccess =json => ({
     type:LOGIN_SUCCESS,
     payload:json,
});

export const loginError = error => ({
  type: LOGIN_ERROR,
  payload: error,
});


export const loginApp = () =>{
    
   return async dispatch =>{
     try{

         dispatch(loginSuccess(true));
      
      
     }catch(error){
        dispatch(loginError(false));
        ToastAndroid.showWithGravity(
          'Network Error',
          ToastAndroid.SHORT,
          ToastAndroid.BOTTOM,
        );
     }
   
   };
  } ;




