import {GET_USER_SCHEDULES_ERROR,GET_USER_SCHEDULES_SUCCESS,GET_USER_SCHEDULES_REQUEST} from '../../constants/reduxConstants';
import axios from 'axios';
import {Alert,AsyncStorage} from 'react-native'



export const getScheduleSuccess =json => ({
     type:GET_USER_SCHEDULES_SUCCESS,
     payload:json,
});

export const getScheduleError = error => ({
  type:GET_USER_SCHEDULES_ERROR,
  payload: error,
});

export const getscheduleRequest = ()=>(
    {
        type: GET_USER_SCHEDULES_REQUEST
    }
);



export const getUserSchedules = () =>{
    
    return async dispatch =>{

    try{
           
                   const token = await AsyncStorage.getItem('userToken')
                   dispatch(getscheduleRequest());
                  

                  axios({
                    method: 'get',
                    url: 'https://dna4wheels-staging.herokuapp.com/schedule/user',
                    headers:{
                      'x-access-token':token,
                  }
                  }) .then(response=> {
                 
                   
                    let arr = response.data.data;
                    let newArr = [];
                   
                
                   
                     if(arr === undefined || arr.length == 0){
                      dispatch(getScheduleSuccess(newArr));
                    }

                    else{
                        

                      for (var i = 0; i < arr.length; i++){
                        var obj = arr[i].info;
                        var b =
                          {
                            "Car":"Car Schedule "+ i,
                             "id": obj._id,
                             "id_Key": i.toString(),
                             "date":obj.dateRequested,
                             "description":obj.description,
                             "status":obj.status,
                             "autoshop":obj.companyId
                           }
                        newArr.push(b)	
                      }
                      dispatch(getScheduleSuccess(newArr));
                    }

                }).catch(error =>{
                     
                      dispatch(getScheduleError(error))
                      console.log(error)
                    })
              
    }catch(error){
        dispatch(getScheduleError(error))
        console.log(error)
     }};

     };
    
    



  






























export const pushSuccess =json => ({
  type:PUSH_SUCCESS,
  payload:json,
});

export const pushError = error => ({
type: PUSH_ERROR,
payload: error,
});


/**
 * Send device token to database
 */


export const registerForNotification = (name) =>{
    
  return async dispatch =>{
    try{

     const value = await AsyncStorage.getItem('deviceIDToken');

     const details = {
       token: value,
       userID: name
     }
     
      
   let response =  fetch(PUSH_ENDPOINT, {
                method: 'POST',
                headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                },
              body: JSON.stringify(details),
	});
  
       let json = await response.json();    
      
    }catch(error){
       dispatch(fetchEventHistoryError(error));
       ToastAndroid.showWithGravity(
         'Network Error',
         ToastAndroid.SHORT,
         ToastAndroid.BOTTOM,
       );
    }
  
  };
 } ;



