import {SCHEDULE_CAR_ERROR,SCHEDULE_CAR_REQUEST,SCHEDULE_CAR_SUCCESS} from '../../constants/reduxConstants';
import {Alert,AsyncStorage} from 'react-native'
import axios from 'axios';



export const scheduleCarSuccess =json => ({
     type:SCHEDULE_CAR_SUCCESS,
     payload:json,
});

export const scheduleCarError = error => ({
  type: SCHEDULE_CAR_ERROR,
  payload: error,
});

export const scheduleRequest = ()=>(
    {
        type: SCHEDULE_CAR_REQUEST
    }
);





export const scheduleCar = (userData) =>{
    
    return async dispatch =>{

        try{
               
                      dispatch(scheduleRequest());
                   
                      const token = await AsyncStorage.getItem('userToken')
    
                      axios({
                        method: 'post',
                        url: 'https://dna4wheels-staging.herokuapp.com/schedule/request',
                        data: userData,
                        headers:{
                            'x-access-token':token,
                        }
                      }) 
                       .then(response=> {
                             
                        if(response.status === 200){
                            dispatch(scheduleCarSuccess('Car Updated Successfully'));
                            Alert.alert('Auto-maintenance','Scheduled successfully at ' + userData.dateRequested)
                        }
                        else{
                           
                            dispatch(scheduleCarError(false))
                            Alert.alert('','Unable to schedule maintenance for this car')
                        }
                
                      }).catch(error =>{
                       
                         dispatch(scheduleCarError(false))
                         Alert.alert('','Unable to schedule maintenance for this car')
                        })
                  
        }catch(error){
              dispatch(scheduleCarError(error));
         }};
    
         };
        
        
    
    
    
      
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    