import {GET_AUTOSHOPS_ERROR,GET_AUTOSHOPS_SUCCESS} from '../../constants/reduxConstants';
import axios from 'axios';
import {Alert,AsyncStorage} from 'react-native'



export const getAutoShopSuccess =json => ({
     type:GET_AUTOSHOPS_SUCCESS,
     payload:json,
});

export const getAutoShopError = error => ({
  type:GET_AUTOSHOPS_ERROR,
  payload: error,
});


export const getAutoShops = () =>{
    
    return async dispatch =>{

    try{
           
                

                  axios({
                    method: 'get',
                    url: 'https://dna4wheels-staging.herokuapp.com/auth/auto',
                
                  }) .then(response=> {
                        
                    let arr =  response.data.users;
                    let newArr=[];
                    
                    for (var i = 0; i < arr.length; i++){
                      var obj = arr[i];
                      var b =
                        {
                          "companyDesc":obj.companyName + '\n '+ obj.address + ' '+ obj.city,
                           "id": obj._id,
                         }
                      newArr.push(b)	
                    }
                
                  
                    dispatch(getAutoShopSuccess(newArr));
                       
                  }).catch(error =>{
                     console.log(error)
                     dispatch(getAutoShopError(error))
                    })
              
    }catch(error){
        dispatch(getAutoShopError(error))
     }};

     };
    
    



  




























