import {ADD_CAR_ERROR,ADD_CAR_SUCCESS,ADD_CAR_REQUEST} from '../../constants/reduxConstants';
import {Alert,AsyncStorage} from 'react-native'
import axios from 'axios';



export const addCarSuccess =json => ({
     type:ADD_CAR_SUCCESS,
     payload:json,
});

export const addCarError = error => ({
  type: ADD_CAR_ERROR,
  payload: error,
});

export const addCarRequest = ()=>(
    {
        type: ADD_CAR_REQUEST
    }
);





export const addCarDetails = (userData) =>{
    
    return async dispatch =>{

        try{
               
                      dispatch(addCarRequest());
                    
                      const token = await AsyncStorage.getItem('userToken')
    
                      axios({
                        method: 'post',
                        url: 'https://dna4wheels-staging.herokuapp.com/car/create',
                        data: userData,
                        headers:{
                            'x-access-token':token,
                        }
                      }) 
                       .then(response=> {
                           
                             dispatch(addCarSuccess('Car Updated Successfully'));
                             Alert.alert('','Car Updated Successfully')
                      }).catch(error =>{
                         dispatch(addCarError(false))
                         Alert.alert('','Unable to add car')
                        })
                  
        }catch(error){
              dispatch(addCarError(error));
         }};
    
         };
        
        
    
    
    
      
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    