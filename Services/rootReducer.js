
/**
 * @class rootReducer
 * It manages all reducers in the state
 */

import {combineReducers} from 'redux';
import loginReducer from './ApiStateReducer/Login'
import ProfileReducer from './ApiStateReducer/Profile'
import decodeReducer  from './ApiStateReducer/decoder'
import addCarReducer from './ApiStateReducer/addCar'
import getCarReducer  from './ApiStateReducer/getCar'
import getAutosReducer from './ApiStateReducer/getAutoshops'
import scheduleCarReducer from './ApiStateReducer/addService'
import getScheduleReducer  from './ApiStateReducer/getUserSchedules'







const appReducer = combineReducers({
  appLogin: loginReducer,
  createProfile: ProfileReducer,
  vinDetail:decodeReducer,
  addCarResponse:addCarReducer,
  carList:getCarReducer,
  carAutos:getAutosReducer,
  scheduleResponse:scheduleCarReducer,
  schedules:getScheduleReducer

});



export default rootReducers = (state,action) =>{
    
    if(action.type === 'RESET'){

        state = undefined
    }

    return appReducer(state,action)
}