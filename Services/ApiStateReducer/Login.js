/**
 * Reducer updates state with to app navigation
 * or Error message
 */

import {LOGIN_ERROR,LOGIN_REQUEST,LOGIN_SUCCESS} from '../../constants/reduxConstants';



const initialState ={
  isLoggedIn:false,
  token:''
};


const LoginReducer = (state = initialState, action) =>{
   
 
    switch(action.type){


         case LOGIN_ERROR:
              return{...state, isLoggedIn:false};

         case LOGIN_SUCCESS:
         
              return {...state, token:action.payload,isLoggedIn:true};

         default:
            return state;     
    }

};
 export default  LoginReducer;