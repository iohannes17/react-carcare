
/**
 * @exports decodeReducer
 * Teaser Details Reducer
 * Updates states in store
 */


import {DECODE_REQUEST,DECODER_ERROR,DECODER_SUCCESS} from '../../constants/reduxConstants';

const initialState ={
   teaserList:'',
   isAvailable: false,
   isFetching: false,
   teaserRequest:false,
  
};


const decodeReducer = (state = initialState, action) =>{
    
  
 
    switch(action.type){


         case DECODER_ERROR:
              return{...state, teaserList:action.payload, isAvailable: false,  isFetching:false,};

         case DECODER_SUCCESS:
         
              return {...state,teaserList:action.payload, isAvailable: true, isFetching: false,};
          
        case DECODE_REQUEST:
              return{ ...state,  isFetching:true,};

         default:
            return state;     
    }

};
 export default decodeReducer;