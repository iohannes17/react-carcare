/**
 * Reducer updates state with to app navigation
 * or Error message
 */

import {ADD_CAR_ERROR,ADD_CAR_REQUEST,ADD_CAR_SUCCESS} from '../../constants/reduxConstants';



const initialState ={
  carResponse:'',
  isAddingCar:false
};


const addCarReducer = (state = initialState, action) =>{
   
 
    switch(action.type){
         
          case ADD_CAR_REQUEST:
              return{...state, isAddingCar:true};

         case ADD_CAR_ERROR:
              return{...state, isAddingCar:false,carResponse:action.payload};

         case ADD_CAR_SUCCESS:
         
              return {...state, isAddingCar:false, carResponse:action.payload};

         default:
            return state;     
    }

};
 export default  addCarReducer;