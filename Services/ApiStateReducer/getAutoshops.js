/**
 * Reducer updates state with to app navigation
 * or Error message
 */

import {GET_AUTOSHOPS_ERROR,GET_AUTOSHOPS_SUCCESS} from '../../constants/reduxConstants';



const initialState ={
  autoShops:[],
  fetchingCar:false
};


const getAutosReducer = (state = initialState, action) =>{
   
 
    switch(action.type){
         
          case GET_AUTOSHOPS_ERROR:
              return{...state, fetchingCar:true,usersCar:''};

         case GET_AUTOSHOPS_SUCCESS:
              return{...state, fetchingCar:false,autoShops:action.payload};

         default:
            return state;     
    }

};
 export default getAutosReducer;