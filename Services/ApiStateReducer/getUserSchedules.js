/**
 * Reducer updates state with to app navigation
 * or Error message
 */

import {GET_USER_SCHEDULES_ERROR,GET_USER_SCHEDULES_REQUEST,GET_USER_SCHEDULES_SUCCESS} from '../../constants/reduxConstants';



const initialState ={
  userSched:[],
  isFetching:false
};


const getScheduleReducer = (state = initialState, action) =>{
   
 
    switch(action.type){
         
          case GET_USER_SCHEDULES_REQUEST:
              return{...state, isFetching:true};

         case GET_USER_SCHEDULES_ERROR:
              return{...state, isFetching:false,};

         case GET_USER_SCHEDULES_SUCCESS:
              return{...state, isFetching:false,userSched:action.payload};
      

         default:
            return state;     
    }

};
 export default getScheduleReducer;