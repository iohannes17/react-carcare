/**
 * Reducer updates state with to app navigation
 * or Error message
 */

import {CREATE_PROFILE_ERROR,CREATE_PROFILE_REQUEST,CREATE_PROFILE_SUCCESS} from '../../constants/reduxConstants';



const initialState ={
  profileResponse:false,
  isCreating:false
};


const ProfileReducer = (state = initialState, action) =>{
   
 
    switch(action.type){
         
          case CREATE_PROFILE_REQUEST:
              return{...state, isCreating:true};

         case CREATE_PROFILE_ERROR:
              return{...state, isCreating:false,profileResponse:action.payload};

         case CREATE_PROFILE_SUCCESS:
         
              return {...state, isCreating:false, profileResponse:action.payload};

         default:
            return state;     
    }

};
 export default  ProfileReducer;