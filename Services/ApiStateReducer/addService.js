/**
 * Reducer updates state with to app navigation
 * or Error message
 */

import {SCHEDULE_CAR_ERROR,SCHEDULE_CAR_REQUEST,SCHEDULE_CAR_SUCCESS} from '../../constants/reduxConstants';



const initialState ={
  response:'',
  isScheduling:false
};


const scheduleCarReducer = (state = initialState, action) =>{
   
 
    switch(action.type){
         
          case SCHEDULE_CAR_REQUEST:
              return{...state, isScheduling:true};

         case SCHEDULE_CAR_SUCCESS:
              return{...state, isScheduling:false,response:action.payload};

         case SCHEDULE_CAR_ERROR:
         
              return {...state,isScheduling:false,response:action.payload};

         default:
            return state;     
    }

};
 export default scheduleCarReducer ;