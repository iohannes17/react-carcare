/**
 * Reducer updates state with to app navigation
 * or Error message
 */

import {GET_CAR_ERROR,GET_CAR_SUCCESS} from '../../constants/reduxConstants';



const initialState ={
  usersCar:[],
  fetchingCar:false
};


const getCarReducer = (state = initialState, action) =>{
   
 
    switch(action.type){
         
          case GET_CAR_ERROR:
              return{...state, fetchingCar:true,usersCar:''};

         case GET_CAR_SUCCESS:
              return{...state, fetchingCar:false,usersCar:action.payload};

         default:
            return state;     
    }

};
 export default  getCarReducer;