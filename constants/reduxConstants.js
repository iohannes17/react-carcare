
/**
 * Base URL
 */

 export const BASE_API = 'https://api.carfacts.ng/api/';
/**
 * Login Constants
 */
export const LOGIN_REQUEST = 'LOGIN_REQUEST';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS'
export const LOGIN_ERROR = 'LOGIN_ERROR';

/**
 * Push Notifications Constants
 */
export const PUSH_REQUEST = 'PUSH_REQUEST';
export const PUSH_SUCCESS = 'PUSH_SUCCESS'
export const PUSH_ERROR = 'PUSH_ERROR';

/**
 * Push Notifications Constants
 */
export const CREATE_PROFILE_REQUEST = 'CREATE_PROFILE_REQUEST';
export const CREATE_PROFILE_SUCCESS = 'CREATE_PROFILE_SUCCESS'
export const CREATE_PROFILE_ERROR = 'CREATE_PROFILE_ERROR';

/**
 * decoder Constants
 */
export const DECODE_REQUEST = 'DECODE_REQUEST';
export const DECODER_SUCCESS = 'DECODER_SUCCESS'
export const DECODER_ERROR = 'DECODER_ERROR';


/**
 * Add car Constants
 */
export const ADD_CAR_REQUEST = 'ADD_CAR_REQUEST';
export const ADD_CAR_SUCCESS = 'ADD_CAR_SUCCESS'
export const ADD_CAR_ERROR = 'ADD_CAR_ERROR';


/**
 * schedule car Constants
 */
export const SCHEDULE_CAR_REQUEST = 'SCHEDULE_CAR_REQUEST';
export const SCHEDULE_CAR_SUCCESS = 'SCHEDULE_CAR_SUCCESS'
export const SCHEDULE_CAR_ERROR = 'SCHEDULE_CAR_ERROR';


/**
 * get user car Constants
 */
export const GET_CAR_SUCCESS = 'GET_CAR_SUCCESS'
export const GET_CAR_ERROR = 'GET_CAR_ERROR';


/**
 * get user car Constants
 */
export const GET_AUTOSHOPS_ERROR = 'GET_AUTOSHOPS_ERROR'
export const GET_AUTOSHOPS_SUCCESS = 'GET_AUTOSHOPS_SUCCESS';



/**
 * get user car schedules
 */
export const GET_USER_SCHEDULES_ERROR = 'GET_USER_SCHEDULES_ERROR '
export const GET_USER_SCHEDULES_SUCCESS = 'GET_USER_SCHEDULES_SUCCESS';
export const GET_USER_SCHEDULES_REQUEST = 'GET_USER_SCHEDULES_REQUEST';








