import React from 'react';
import PropTypes from 'prop-types';
import {Text,View,TouchableOpacity,Alert,Image,StatusBar,AsyncStorage} from 'react-native'
import * as Facebook from 'expo-facebook';
import {connect} from 'react-redux';
import {loginApp} from '../../Services/ApiActions/Login';
import * as Google from 'expo-google-app-auth';
import styles from './styles';
import {LinearGradient} from 'expo-linear-gradient';
import icon from '../../assets/images/icon.png'
import facebook from '../../assets/images/facebook.png'
import google from '../../assets/images/google.png'
import Toast,{DURATION} from 'react-native-easy-toast'









class LoginScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
        
    }


  

    logIn = async()=> {
      try {
        const {
          type,
          token,
          expires,
          permissions,
          declinedPermissions,
        } = await Facebook.logInWithReadPermissionsAsync('2351626564954744', {
          permissions: ['public_profile','email'],
        });
        if (type === 'success') {
          // Get the user's name using Facebook's Graph API
          const response = await fetch(`https://graph.facebook.com/me?access_token=${token}`);
          const response2 = await fetch(
            `https://graph.facebook.com/me?fields=id,name,email,birthday&access_token=${token}`);
            AsyncStorage.setItem('facebook_token',token)
         
        
          //fetching converting response to json
          let res = (await response.json())
          let res2 = (await response2.json())

          //saving user name in storage
          Alert.alert('Logged in!', `Hi ${res.name}!`);  
          AsyncStorage.setItem('user_name',res.name);
          AsyncStorage.setItem('user_email',res2.email)

          this.props.loginApp();

            //computing user login details email and token
          const data={
            username: res2.email,
            password: await AsyncStorage.getItem('tokenkey')
         }

      


        } else {
          // type === 'cancel'
        }
      } catch ({ message }) {
        alert(`Facebook Login Error: ${message}`);
      }
    }



GoogleSignIn = async()=>{

const config={
  androidClientId:'740670221243-agivc6rnr2a9egm0f3d8bp3247pb1bm2.apps.googleusercontent.com',
  iosClientId: '740670221243-to5inqdiuetgn1qv710575vabvoh5ihr.apps.googleusercontent.com',
  androidStandaloneAppClientId :'740670221243-agivc6rnr2a9egm0f3d8bp3247pb1bm2.apps.googleusercontent.com',
  scopes: ['profile', 'email'],
  behavior:'web'
}


// AIzaSyCfa4EXCqTZOelN16jcgWrJIjGddp6yxNk

  

   try{
       const { type, accessToken, user } = await Google.logInAsync(config);

  if (type === 'success') {
    // Then you can use the Google REST API
     
    Alert.alert('Logged in!', `Hi ${(user.name)}!`);
    AsyncStorage.setItem('user_name',user.name)
    AsyncStorage.setItem('user_email', user.email,)
    AsyncStorage.setItem('google_token',accessToken)
         

    this.props.loginApp();


    } else {
          return { cancelled: true };
  }

}catch(error){
  return { error: true };
}


}


// async componentDidMount(){
//   const fbToken = await AsyncStorage.getItem('facebook_token');
//   const google_token = await AsyncStorage.getItem('google_token');

//   if(google_token != null || fbToken != null){
//     this.props.loginApp();
//   }
// }
















    render() {
        return (
            <View style={styles.container}>
             <StatusBar hidden/>

              <View style={{backgroundColor:'#373f4b',height:550,width:450,transform:[{rotate:"45deg"}],top:-500,position:'absolute',left:-115,
                 borderRadius:50,elevation:2}}>  

                         <Image source={icon}      
                         style={{right:4,position:'absolute',bottom:5,width:65,
                         height:65,transform:[{rotate:"317deg"}]}}/>
                   </View> 
                  
            
              <View style={{height:550,width:450,transform:[{rotate:"45deg"}],top:-450,position:'absolute',left:-90,
                 borderRadius:0,opacity:0.7}}>   

                  </View> 


             <View style={{backgroundColor:'#373f4b',height:500,width:570,transform:[{rotate:"45deg"}],top:0,position:'absolute',right:-300,
                 borderRadius:80,elevation:2}}>

                          <TouchableOpacity
                          onPressIn={this.GoogleSignIn}
                           style={{
                          zIndex:100,  
                          borderBottomRightRadius:26,
                          borderTopRightRadius:26,
                          transform:[{rotate:"135deg"}],                        
                          height:50,width:250,position:'absolute',left:50,top:245}}>
                            
                            <Image source={google}      
                              style={{
                              height:50,width:250,
                  
                              transform:[{rotate:"180deg"}]}}/>
                          </TouchableOpacity>


                           <TouchableOpacity
                          onPressIn={this.logIn}
                           style={{
                          zIndex:100,  
                          borderBottomRightRadius:26,
                          borderTopRightRadius:26,
                          transform:[{rotate:"135deg"}],                        
                          height:50,width:250,position:'absolute',left:80,top:330}}>
                            
                            <Image source={facebook}      
                              style={{
                              height:50,width:250,
                  
                              transform:[{rotate:"180deg"}]}}/>
                            
                          </TouchableOpacity>
             </View> 

               <View style={{backgroundColor:'#00c9ff',height:200,width:200,transform:[{rotate:"45deg"}],top:380,position:'absolute',right:-53,
                 borderRadius:40,opacity:0.53,elevation:2}}>

             </View> 




               <View style={{backgroundColor:'#373f4b',bottom:-150,height:290,width:290,transform:[{rotate:"45deg"}],position:'absolute',borderRadius:60,left:-10}}>

              </View>  

              <LinearGradient
														colors={['#00C9FF','#92FE9D']}
														start={[0.0, 1.0]}
														end={[0.9, 0.6]}
														locations={[0.0, 1.0]}
														style={{bottom:-200,height:400,width:400,transform:[{rotate:"45deg"}],position:'absolute',borderRadius:60,left:-200}}>
           	</LinearGradient>		
               

               <Toast ref="toast"
                style={{backgroundColor:'#000000'}}
                position="bottom"
                fadeInDuration={750}
                fadeOutDuration={1000}
                opacity={0.8}
                textStyle={{color:'#00C9FF'}}
            />
            
                         
            </View>
        );
    }
}


const mapStateToProps = state =>{
	return{
		
		
	};
  }
  
  LoginScreen.propTypes ={
	loginApp:PropTypes.func.isRequired
  }

  export default connect(mapStateToProps, {loginApp})(LoginScreen);
