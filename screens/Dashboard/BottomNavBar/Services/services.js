//
//  AirtimeAndBills
//  Cryptonance
//
//  Created by Cryptonance.
//  Copyright © 2018 Cryptonance. All rights reserved.
//

import React from "react"
import { Image, TouchableOpacity, StyleSheet, View, Text,ImageBackground,Picker,ScrollView,ActivityIndicator,AsyncStorage } from "react-native"
import styles from './styles'
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from 'react-native-responsive-screen'
import {DatePicker,Item,Label,Input} from 'native-base'
import {LinearGradient} from 'expo-linear-gradient';
import Icon from 'react-native-vector-icons/Entypo'
import Toast,{DURATION} from 'react-native-easy-toast'
import {connect} from 'react-redux';
import {getUserCars} from '../../../../Services/ApiActions/getCars';
import {getAutoShops} from '../../../../Services/ApiActions/getAutoshops'
import {scheduleCar} from '../../../../Services/ApiActions/addService'
import PropTypes from 'prop-types'





class Services extends React.Component {

	static navigationOptions = ({ navigation }) => {
	
		const { params = {} } = navigation.state
		return {
				header: null,
				headerLeft: null,
				headerRight: null,
			}
	}

	
	constructor(props) {
		super(props);
		this.state = {
		  car: 'Select Car',
		  chosenDate: new Date(),
		  description:'',
		  autoshop:'Select AutoShop',

		};
		
		this.setDate = this.setDate.bind(this);
	  }


     componentDidMount(){
		 this.props.getUserCars();
		 this.props.getAutoShops()
	 }


	  onAutoShopChange(value) {
		this.setState({
		  autoshop: value
		});
	  }

	  onCarChange(value) {
		this.setState({
		  car: value
		});
	  }

	  setDate(newDate) {
		this.setState({ chosenDate: newDate });
	  }





	  _onSubmit = async ()=>{
		  
		try{
			 const activated =  await AsyncStorage.getItem('activated');
			 const description = this.state.description;
			 const car = this.state.car;
			 const autoshop = this.state.autoshop;
			 const chosendate = this.state.chosenDate;

			 if(activated === 'activated'){
				
				if(description === ''){
					this.refs.toast.show('Please fill in description');
				}
				else if(chosendate === undefined){
							  
					this.refs.toast.show('Please select all field');
				}
				else{

					const data ={

						companyId:this.state.autoshop,
						carId:this.state.car,	
						dateRequested:this.state.chosenDate,
						description:this.state.description
					}

                      this.props.scheduleCar(data);

				}
				
			 }

		  else{
			this.refs.toast.show('Please update your profile');

	   }}
		 catch(e){
			this.refs.toast.show('Please update your profile');

	   }
		 
   }
	 








	render() {

		let arr = this.props.usersCarList === '' ? [] : this.props.usersCarList
	
	

	
		return  <View Style={styles.container}>
		           
		<View style={{height:hp('2%'),width:wp('100%'),backgroundColor:'#101112'}}></View>
		   
		<ImageBackground source={require('../../../../assets/images/bg.png')} style={{
									 width:wp('100%'), 
									 height:hp('32'),
									 alignItems:'center',
									 justifyContent:'center',
									 padding:3
									 }}>

									 <Text style={{textAlign:'left',fontFamily:'Poppins-Bold',fontSize:25,color:'#ffffff',left:5,position:'absolute'}}>
				                            {'\n'}Schedule a car date{'\n'} with top Nigerian Autoshops</Text>
									 
			</ImageBackground>   
					<ScrollView contentContainerStyle={{alignItems:'center', backgroundColor:'#101112', height:hp('90%')}}>

							<View style={{width:wp('100%'), height:hp('3%')}}></View>	
					  		
							<LinearGradient
							  colors={['#00C9FF','#92FE9D']}
							  start={[0.0, 0.9]}
							  end={[1.0, 0.4]}
							  locations={[0.0, 1.0]}
							  style={styles.innerroundShape}>

                            
                              {/* User selects vehicle for service */}

						  <Picker
						  mode="dropdown"
						  iosIcon={<Icon name="arrow-down" />}
						  style={{ 
							  width:wp('70%'),
							  alignSelf:'center',
							  borderColor:'#92FE9D',
							   borderRadius:10,
							  opacity:0.4}}
						  placeholder="Select Vehicle"
						  placeholderStyle={{ color: "green" }}
						  selectedValue={this.state.car}
						  onValueChange={this.onCarChange.bind(this)}>	

					{
						 arr.map((item) =>{
						return(
							<Picker.Item  label={item.brand} value={item.id} key={item.id}/>
							);
							})
               }
				 
				  </Picker>																	  
			     </LinearGradient>			 

				 <View style={{width:wp('100%'), height:hp('2%')}}></View>	
        

				      {/* Service description input field */}

				<Item stackedLabel style={styles.itemStyle}>
					<Label style={{padding:10,color:'#92FE9D',fontSize:9,fontFamily:'Poppins-Bold'}}>Service Description</Label>
					<Input style={{color:'#ffffff',paddingLeft:10}} multiline={true} numberOfLines={5} 
					 onChangeText={(value)=> this.setState({description:value})} value={this.state.description}/>
					</Item>

				 <View style={{width:wp('100%'), height:hp('2%')}}></View>	

                             

				<LinearGradient
					colors={['#00C9FF','#92FE9D']}
					start={[0.0, 0.9]}
					end={[1.0, 0.4]}
					locations={[0.0, 1.0]}
					style={styles.innerroundShape}>
                     <Picker
						  mode="dropdown"
						  iosIcon={<Icon name="arrow-down" />}
						  style={{ 
							  width:wp('70%'),
							  alignSelf:'center',
							  borderColor:'#92FE9D',
							
							  borderRadius:10,
							  opacity:0.4}}
						  placeholder="Select AutoShop"
						  placeholderStyle={{ color: "green" }}
						  selectedValue={this.state.autoshop}
						  onValueChange={this.onAutoShopChange.bind(this)}>

					        	
						 {this.props.autoList.map((item) =>{
					        return(
								
							    <Picker.Item label={item.companyDesc} value={item.id} key={item.id}/>);})}
						
				  </Picker>																	  
			     </LinearGradient>	
				

						   <View style={{width:wp('100%'), height:hp('2%')}}></View>	
                  
                            
							    {/* Picker Component for  to User service date */}
							<LinearGradient
							  colors={['#00C9FF','#92FE9D']}
							  start={[0.0, 0.9]}
							  end={[1.0, 0.4]}
							  locations={[0.0, 1.0]}
							  style={styles.innerroundShape}>

								<DatePicker
								defaultDate={new Date(2019, 10, 10)}
								minimumDate={new Date(2019, 10, 10)}
								maximumDate={new Date(2050, 12, 31)}
								locale={"en"}
								timeZoneOffsetInMinutes={undefined}
								modalTransparent={true}
								animationType={"fade"}
								androidMode={"default"}
								placeHolderText="Service Date"
								textStyle={{ color: "#000000" }}
								placeHolderTextStyle={{ color: "#101112" }}
								onDateChange={this.setDate}
								disabled={false}
								/>
																					  
			                 </LinearGradient>	
				       <View style={{width:wp('100%'), height:hp('2%')}}></View>	
                            
							
				 	 <TouchableOpacity style={styles.buttonView} onPress={this._onSubmit}>
					<LinearGradient
							colors={['#00C9FF','#92FE9D']}
							start={[0.0, 0.9]}
							end={[1.0, 0.4]}
							locations={[0.0, 1.0]}
							style={styles.gradientStyle}>
								
				{this.props.isScheduling ? <ActivityIndicator size={32} color="white"/>
				                            :	
		         <Text style={styles.iconText}>Submit</Text>}
																	
							</LinearGradient>
                          </TouchableOpacity>	

						
					</ScrollView>  	
					<Toast ref="toast"
						style={{backgroundColor:'#000000'}}
						position="bottom"
						fadeInDuration={1050}
						fadeOutDuration={3000}
						opacity={0.8}
						textStyle={{color:'#00C9FF'}}
						/>	
						
                 </View>
				
	}

}


const mapStateToProps = state =>{
	return{
	  usersCarList:state.carList.usersCar,
	  autoList:state.carAutos.autoShops,
	  isScheduling:state.scheduleResponse.isScheduling
		
	};
  }
  
  Services.propTypes ={
	getUserCars:PropTypes.func.isRequired,
	getAutoShops:PropTypes.func.isRequired,
	scheduleCar:PropTypes.func.isRequired,
	usersCarList:PropTypes.any,
	autoList:PropTypes.any,
	isScheduling:PropTypes.bool

  }

  export default connect(mapStateToProps, {getUserCars,getAutoShops,scheduleCar})(Services);








