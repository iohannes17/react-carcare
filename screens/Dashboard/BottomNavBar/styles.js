import {StyleSheet} from 'react-native';
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from 'react-native-responsive-screen';

export default StyleSheet.create({
	dashboardView: {
	    backgroundColor: "#1a1c1f",
		
	
		
	},

	rowView:{
		
		
		width:wp('100%'),
		flexDirection:'row',
		justifyContent:'center',
		alignContent:'center',
		alignSelf:'center',
		backgroundColor:'#101112'
	},
	
	bigView:{
		
		borderBottomColor:'#1a1c1f',
		borderBottomWidth:1,
		backgroundColor:'#101112',
		height:hp('65%'),
	},

	buttonView:{
		width:wp('40%'),
		height:hp('25%')
	},
	gradientStyle:{
		 alignItems: 'center',
		  borderRadius: 20,
		  width:wp('40%'),
		  height:hp('25%'),
		  justifyContent:'center',
		  alignContent:'center'
	},

	iconStyle:{
		width:70,
		height:45,
		marginTop:20
	},

	iconStyle2:{
		width:50,
		height:50,
		marginTop:15
	},

	iconStyle3:{
		width:60,
		height:60,
		marginTop:15
	},


	iconStyle4:{
		width:75,
		height:57,
		marginTop:15
	},




	iconText:{
		 color:'#101112',
		 fontSize: 10,
		 fontFamily:'Lato-Bold',
		 fontWeight:'900',
		 textAlign:'center',
		 marginTop:10
	},

	subiconText:{
		color:'#101112',
		fontSize: 10,
		fontFamily:'Lato-Regular',
		fontWeight:'bold',
		textAlign:'center',
		marginTop:10
   },

   roundShape:{
    width: 70,
	height:70,
    borderRadius: 70/2,
	backgroundColor: 'transparent',
	borderColor:'#92FE9D',
	alignSelf:'center',
	borderWidth:1,
	marginTop:20,
	justifyContent:'center'

	},
	
	innerroundShape:{
		width: 50,
		height:50,
		borderRadius: 50/2,
		backgroundColor: 'transparent',
		alignSelf:'center',
		padding:5,
		alignSelf:'center',
		justifyContent:'center',
		alignContent:'center',
		alignItems:'center'
		},
   
	
})
