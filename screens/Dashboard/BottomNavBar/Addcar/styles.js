import {StyleSheet} from 'react-native';
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from 'react-native-responsive-screen'



export default StyleSheet.create({
	container: {
       
        backgroundColor:'#101112',
        alignItems: 'center',
        justifyContent: 'center',
      },
	

    
    innerroundShape:{
        width: wp('70%'),
		borderRadius: 10,
		alignSelf:'center',
		alignSelf:'center',
		justifyContent:'center',
		alignContent:'center'
        },
        
        itemStyle:{
            borderColor:'#373f4b',
            borderWidth:2,
            backgroundColor:'#1a1c1f',
            height:hp('3%'),
            width:wp('90'),
            borderRadius:10,
},
buttonView:{
    width:wp('40%'),
    height:hp('5%'),
    borderRadius: 10,
},
gradientStyle:{
     alignItems: 'center',
      borderRadius: 10,
      width:wp('40%'),
      height:hp('5%'),
      justifyContent:'center',
      alignContent:'center'
},
iconText:{
    color:'#101112',
    fontSize: 10,
    fontFamily:'Poppins-Bold',
    fontWeight:'900',
    textAlign:'center',
}

	
})
