//Car Care
//Add car Page
//  Copyright © 2018 Cryptonance. All rights reserved.
//

import { View, Image, TouchableOpacity, 
	Switch, StyleSheet, Text,ImageBackground,ScrollView,Picker,AsyncStorage,ActivityIndicator } from "react-native"
import Icon from 'react-native-vector-icons/Entypo'
import styles from './styles'
import React from "react"
import DashContainer from '../../../../components/DashboardContainer'
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {Form, Item, Input, Label} from 'native-base';
import {LinearGradient} from 'expo-linear-gradient';
import Toast,{DURATION} from 'react-native-easy-toast'
import {connect} from 'react-redux';
import {addCarDetails} from '../../../../Services/ApiActions/addCar';
import PropTypes from 'prop-types'







 class Addcar extends React.Component {

	static navigationOptions = ({ navigation }) => {
	
		const { params = {} } = navigation.state
		return {
				header: null,
				headerLeft: null,
				headerRight: null,
			}
	}

	
	constructor(props) {
		super(props);
		this.state = {
		  selected2:'PMS',
		  correctVal: '',
		  transmission:'Automatic',
		  vin:'',
		  brand:'',
		  model:'',
		  color:'',
		  mileage:'',
		  fname:'',
		  phone:'',
		  email:'',
		  address:''
		};
	  }


	  onValueChange2(value) {
		this.setState({
		  selected2: value
		});
	  }


	  onTransmissionChange(value) {
		this.setState({
		  transmission: value
		});
	  }


	  async componentDidMount (){
		 
		this.setState({
			fname: await AsyncStorage.getItem('fullname'),
			phone: await AsyncStorage.getItem('phone'),
			email: await AsyncStorage.getItem('email'),
			address: await AsyncStorage.getItem('address')		
		});
	  }


	  _onSubmit = async()=>{

		  
         try{
			  const activated =  await AsyncStorage.getItem('activated');

			  if(activated === 'activated'){

				if(this.state.vin.length < 17 ){
			 
					this.refs.toast.show('VIN/Chassis Incorrect');

				}
				else if(this.state.brand.length < 0 ||
						this.state.model.length < 0 ||
						this.state.color.length < 0 ||
						this.state.mileage.length < 0){
					
							this.refs.toast.show('Please fill all fields');

				}
				else{
					 
	                    	const data ={
							   vin:this.state.vin,
							   model:this.state.model,
							   mileage:this.state.mileage,
							   transmission:this.state.transmission,
							   fuelType:this.state.selected2,
							   carBrand:this.state.brand,
							   color:this.state.color,
		
		                      };
		                this.props.addCarDetails(data)
                 }

			}
			
		   else{
			this.refs.toast.show('Please update your profile');

		   }

		}
		  catch(e){
			this.refs.toast.show('Please update your profile');

			
		}

		this.setState({
			vin:'',
			model:'',
			mileage:'',
			color:'',
			transmission:'Automatic',
			brand:'',
			selected2:'PMS'
		})
		  
	}
	  

	

	render() {
       
	
		return  <View Style={styles.container}>
		           
		<View style={{height:hp('1%'),width:wp('100%'),backgroundColor:'#101112'}}></View>
		  		 <ImageBackground source={require('../../../../assets/images/bg.png')} style={{
						 width:wp('100%'), 
						 height:hp('32%'),
						 alignItems:'center',
						 justifyContent:'center',
						 padding:3
						 }}>
				<Text style={{textAlign:'left',fontFamily:'Poppins-Bold',fontSize:25,color:'#ffffff',left:5,position:'absolute'}}>
				      {'\n'}Keep track of{'\n'}your vehicle information</Text>
						   
					</ImageBackground>      
					<ScrollView contentContainerStyle={{alignItems:'center', backgroundColor:'#101112', }}>

                                                     
							<Item stackedLabel style={styles.itemStyle}>
							<Label style={{padding:10,color:'#92FE9D',fontSize:9,fontFamily:'Poppins-Bold'}}>VIN|Chassis Number</Label>
							<Input style={{color:'#ffffff',paddingLeft:5}} maxLength={17} 
							              onChangeText={(value)=> this.setState({vin:value})} value={this.state.vin}/>
							</Item>
							 
							  <View style={{width:wp('100%'), height:hp('2%')}}></View>	

						   <Item stackedLabel style={styles.itemStyle}>
							<Label style={{padding:10,color:'#92FE9D',fontSize:9,fontFamily:'Poppins-Bold'}}>Car Brand</Label>
							<Input style={{color:'#ffffff',paddingLeft:5}} onChangeText={(value)=> this.setState({brand:value})} value={this.state.brand}/>
							</Item>

							  <View style={{width:wp('100%'), height:hp('2%')}}></View>	

							<Item stackedLabel style={styles.itemStyle}>
							<Label style={{padding:10,color:'#92FE9D',fontSize:9,fontFamily:'Poppins-Bold'}}>Model</Label>
							<Input style={{color:'#ffffff',paddingLeft:5}} onChangeText={(value)=> this.setState({model:value})} value={this.state.model}/>
							</Item>
							

							<View style={{width:wp('100%'), height:hp('3%')}}></View>	
					  		
							<LinearGradient
							  colors={['#00C9FF','#92FE9D']}
							  start={[0.0, 0.9]}
							  end={[1.0, 0.4]}
							  locations={[0.0, 1.0]}
							  style={styles.innerroundShape}>


						  <Picker
						  mode="dropdown"
						  iosIcon={<Icon name="arrow-down" />}
						  style={{ 
							  width:wp('70%'),
							  alignSelf:'center',
							  borderColor:'#92FE9D',

							  borderRadius:10,
							  opacity:0.4}}
						  placeholder="Transmission Type"
						  placeholderStyle={{ color: "green" }}
						  selectedValue={this.state.selected2}
						  itemTextStyle={{color:'green'}}
						  itemStyle={{color:'green'}}
						  
						  onValueChange={this.onTransmissionChange.bind(this)}>

					  
						  <Picker.Item label="Automatic" value="Automatic" />
						  <Picker.Item label="Manual" value="Manual" />
						  <Picker.Item label="Hybrid" value="Hybrid" />
				  </Picker>																	  
			     </LinearGradient>	
                  


                        	<View style={{width:wp('100%'), height:hp('3%')}}></View>	
                           <Form>
							<Item stackedLabel style={styles.itemStyle}>
							<Label style={{padding:10,color:'#92FE9D',fontSize:9,fontFamily:'Poppins-Bold'}}>Color</Label>
							<Input style={{color:'#ffffff',paddingLeft:5}} onChangeText={(value)=> this.setState({color:value})} value={this.state.color}/>
							</Item>

							  <View style={{width:wp('100%'), height:hp('2%')}}></View>	

						   <Item stackedLabel style={styles.itemStyle}>
							<Label style={{padding:10,color:'#92FE9D',fontSize:9,fontFamily:'Poppins-Bold'}}>Mileage</Label>
							<Input style={{color:'#ffffff',paddingLeft:5}} onChangeText={(value)=> this.setState({mileage:value})} value={this.state.mileage}/>
							</Item>

							  <View style={{width:wp('100%'), height:hp('2%')}}></View>	
							</Form>


							<LinearGradient
							  colors={['#00C9FF','#92FE9D']}
							  start={[0.0, 0.9]}
							  end={[1.0, 0.4]}
							  locations={[0.0, 1.0]}
							  style={styles.innerroundShape}>


						  <Picker
						  mode="dropdown"
						  iosIcon={<Icon name="arrow-down" />}
						  style={{ 
							  width:wp('70%'),
							  alignSelf:'center',
							  borderColor:'#92FE9D',
							 
							  borderRadius:10,
							  opacity:0.4}}
						  placeholder="Fuel Type"
						  placeholderStyle={{ color: "green" }}
						  selectedValue={this.state.selected2}
						  itemTextStyle={{color:'green'}}
						  itemStyle={{color:'green'}}
						  
						  onValueChange={this.onValueChange2.bind(this)}>

					  
						  <Picker.Item label="PMS" value="PMS" />
						  <Picker.Item label="Electric" value="Electric" />
						  <Picker.Item label="Diesel" value="Diesel" />
						  <Picker.Item label="Hybrid" value="Hybrid" />
				  </Picker>																	  
			     </LinearGradient>	
				 <View style={{width:wp('100%'), height:hp('3%')}}></View>	

				 	 <TouchableOpacity style={styles.buttonView} onPress={this._onSubmit}>
					<LinearGradient
							colors={['#00C9FF','#92FE9D']}
							start={[0.0, 0.9]}
							end={[1.0, 0.4]}
							locations={[0.0, 1.0]}
							style={styles.gradientStyle}>
								
							{this.props.isAdding ? <ActivityIndicator color="white" size={30}/> :
							<Text style={styles.iconText}>Submit</Text>}
																	
							</LinearGradient>
                          </TouchableOpacity>			
						  <View style={{width:wp('100%'), height:hp('80%')}}></View>	
					</ScrollView>  
					<Toast ref="toast"
						style={{backgroundColor:'#000000'}}
						position="bottom"
						fadeInDuration={1050}
						fadeOutDuration={3000}
						opacity={0.8}
						textStyle={{color:'#00C9FF'}}
						/>
                 </View>
				
	}
}



const mapStateToProps = state =>{
	return{
		isAdding:state.addCarResponse.isAddingCar
		
	};
  }
  
  Addcar.propTypes ={
	addCarDetails:PropTypes.func.isRequired,
	isAdding:PropTypes.bool
  }

  export default connect(mapStateToProps, {addCarDetails})(Addcar);







