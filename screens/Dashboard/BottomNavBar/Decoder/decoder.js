import React, { Component } from 'react'
import {View,Text,TouchableOpacity,KeyboardAvoidingView,ImageBackground,Image,ActivityIndicator} from 'react-native';
import {LinearGradient} from 'expo-linear-gradient';
import styles from './styles';
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {Form, Item, Input, Label } from 'native-base';
import PropTypes from 'prop-types';
import {decodeVin} from '../../../../Services/ApiActions/Decoder'
import {connect} from 'react-redux';
import Toast,{DURATION} from 'react-native-easy-toast'






class VinDecoder extends Component {





  static navigationOptions = ({ navigation }) => {
	
		const { params = {} } = navigation.state
		return {
			headerTitle:'Vin Decoder'
			}
  }
  

	constructor(props) {
		super(props);
		this.state = {
      
      vin:'',
      sequential:''
		};
    }
    

    onDecode=()=>{
      const vinn = this.state.vin;


      if(vinn.length < 17){
        this.refs.toast.show('VIN/Chassis incorrect');
			
      }
      else{
        const sub = vinn.substr(-5)
        this.setState({
          sequential:sub
        })
        this.props.decodeVin(vinn)
      
      }
    
    }



  render() {
          
      const sequential = this.state.sequential;
      const vin = this.props.result.teaserList === '' ? '' : this.props.result.teaserList.attributes.VIN
      const make = this.props.result.teaserList === '' ? '' : this.props.result.teaserList.attributes.Make
      const model = this.props.result.teaserList === '' ? '' : this.props.result.teaserList.attributes.Model
      const year = this.props.result.teaserList === '' ? '' : this.props.result.teaserList.attributes.Style
      const trim = this.props.result.teaserList === '' ? '' : this.props.result.teaserList.attributes.Trim
      const engine = this.props.result.teaserList === '' ? '' : this.props.result.teaserList.attributes.Engine
      const style = this.props.result.teaserList === '' ? '' : this.props.result.teaserList.attributes.Style
    

    return (
        <View Style={styles.container}>
		           
		   
                
            <ImageBackground source={require('../../../../assets/images/bg.png')} style={{
									 width:wp('100%'), 
                   height:hp('21%'),
                   alignItems:'center'
									 }}>

               <Text style={{textAlign:'left',fontFamily:'Poppins-Bold',fontSize:25,color:'#ffffff',left:5,position:'absolute'}}>
				      {'\n'}Get accurate{'\n'}information on any automobile</Text>
                    
								</ImageBackground>  

             <View style={{backgroundColor:'#101112',justifyContent:'center',alignItems:'center' }}>

               <View style={{width:wp('100%'), height:hp('1.5%')}}></View>	

               <Item stackedLabel style={styles.itemStyle}>
							<Label style={{padding:10,color:'#92FE9D',fontSize:12,fontFamily:'Poppins-Bold',letterSpacing:4,textAlign:'center'}}> Enter VIN|Chassis Number</Label>
							<Input style={{color:'#ffffff',paddingLeft:5,letterSpacing:2,textAlign:'center',fontSize:18,fontFamily:'Poppins-Bold'}} maxLength={17}  onChangeText={(value)=> this.setState({vin:value})} value={this.state.vin}/>
							</Item>

                     
                     <View style={{width:wp('100%'), height:hp('3%')}}></View>	


                           <Image source={require('../../../../assets/images/Car.png')} 
                             resizeMode='center'
                            style={{alignSelf:'center',width:300,height:130,top:78,position:'absolute',zIndex:99}}/>    				  
														
                            <View style={{width:wp('100%'), height:hp('4%')}}></View>

                      	<LinearGradient
                        colors={['#00C9FF','#92FE9D']}
                        start={[0.0, 0.9]}
                        end={[1.0, 0.4]}
                        locations={[0.0, 1.0]}
                        style={styles.gradientStyle}>

                         <View style={{width:wp('100%'), height:hp('10%')}}></View>

              <View style={{justifyContent:'center',flexDirection:"row",width:'100%',height:10,marginTop:14}}>
                   <View style={styles.colView}>
                         <Text style ={styles.detailsText}>VIN : </Text></View>

                   <View style={styles.colView}>
                         <Text style={styles.detailsInfoText}>{vin}</Text></View>
              </View>
              
              <View style={{justifyContent:'center',flexDirection:"row",width:'100%',height:10,marginTop:14}}>
                   <View style={styles.colView}>
                         <Text style ={styles.detailsText}>Manufacturer : </Text></View>

                   <View style={styles.colView}>
                         <Text style={styles.detailsInfoText}>{make}</Text></View>
              </View>

               <View style={{justifyContent:'center',flexDirection:"row",width:'100%',height:10,marginTop:14}}>
                   <View style={styles.colView}>
                         <Text style ={styles.detailsText}>Model : </Text></View>

                   <View style={styles.colView}>
                         <Text style={styles.detailsInfoText}>{model}</Text></View>
              </View>

                <View style={{justifyContent:'center',flexDirection:"row",width:'100%',height:10,marginTop:14}}>
                   <View style={styles.colView}>
                         <Text style ={styles.detailsText}>Year : </Text></View>

                   <View style={styles.colView}>
                         <Text style={styles.detailsInfoText}>{year}</Text></View>
              </View>

                 <View style={{justifyContent:'center',flexDirection:"row",width:'100%',height:10,marginTop:14}}>
                   <View style={styles.colView}>
                         <Text style ={styles.detailsText}>Engine Type :</Text></View>

                   <View style={styles.colView}>
                         <Text style={styles.detailsInfoText}>{engine}</Text></View>
              </View>

               <View style={{justifyContent:'center',flexDirection:"row",width:'100%',height:10,marginTop:14}}>
                   <View style={styles.colView}>
                         <Text style ={styles.detailsText}>Style Type :</Text></View>

                   <View style={styles.colView}>
                         <Text style={styles.detailsInfoText}>{style}</Text></View>
              </View>

             <View style={{justifyContent:'center',flexDirection:"row",width:'100%',height:10,marginTop:14}}>
                   <View style={styles.colView}>
                         <Text style ={styles.detailsText}>Trim :</Text></View>

                   <View style={styles.colView}>
                         <Text style={styles.detailsInfoText}>{trim}</Text></View>
              </View>
              
              <View style={{justifyContent:'center',flexDirection:"row",width:'100%',height:10,marginTop:14}}>
                   <View style={styles.colView}>
                         <Text style ={styles.detailsText}>Sequential :</Text></View>

                   <View style={styles.colView}>
                         <Text style={styles.detailsInfoText}>{sequential}</Text></View>
              </View>

                 
    
                         <TouchableOpacity style={styles.buttonView} onPressIn={this.onDecode}>
                              {this.props.result.isFetching  ? 
                               <ActivityIndicator size={15} color="#ffffff"/> :
                              <Text style={styles.buttonText}>Submit</Text>}
                             </TouchableOpacity>			

			          		</LinearGradient>  

                      
                       
             </View>	
                         <Toast ref="toast"
                        style={{backgroundColor:'#000000'}}
                        position="top"
                        fadeInDuration={1050}
                        fadeOutDuration={3000}
                        opacity={0.8}
                        textStyle={{color:'#00C9FF'}}
                        />
       </View>
    )
  }
}

  
const mapStateToProps = state =>{
  return{
     isFetching:state.vinDetail.isFetching,
     isAvailable:state.vinDetail.isAvailable,
     result:state.vinDetail
  };
}

VinDecoder.propTypes ={
 decodeVin:PropTypes.func.isRequired,
 isFetching:PropTypes.bool,
 isAvailable:PropTypes.bool,
 result:PropTypes.any
}



export default connect(mapStateToProps, {decodeVin})(VinDecoder);
