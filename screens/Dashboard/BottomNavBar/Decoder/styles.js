import {StyleSheet} from 'react-native';
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from 'react-native-responsive-screen'



export default StyleSheet.create({
	container: {
       
        backgroundColor:'#101112',
        alignItems: 'center',
        justifyContent: 'center',
       
      },
	

    
    innerroundShape:{
        width: wp('70%'),
		borderRadius: 10,
		alignSelf:'center',
		alignSelf:'center',
		justifyContent:'center',
		alignContent:'center'
        },
        
        itemStyle:{
            borderColor:'#373f4b',
            borderWidth:2,
            backgroundColor:'#1a1c1f',
            height:hp('3%'),
            width:wp('90'),
            borderRadius:10,
},
    buttonView:{
        width:wp('40%'),
        height:hp('5%'),
        borderRadius: 10,
        backgroundColor:'#101112',
        alignSelf:'center',
        marginTop: 25,
        alignItems:'center',
        justifyContent:'center',
        alignContent:'center'
    },


    gradientStyle:{
       
        borderTopRightRadius: 30,
        borderTopLeftRadius:30,
        width:wp('90%'),
        height:hp('60%'),

    },

detailsText:{
    fontFamily:'Poppins-Bold',
    fontSize:13,
    textAlign:'right',
    fontWeight:'900',
    fontStyle:'normal',
    marginTop: 10,
    fontWeight:'900'
},

detailsInfoText:{
    fontFamily:'Poppins-Regular',
    fontSize:12,
    textAlign:'left',
    fontStyle:'normal',
    marginTop: 10,
    color:'#ffffff'

},

buttonText:{
    fontFamily:'Poppins-Bold',
    fontSize:12,
    textAlign:'center',
    fontWeight:'bold',
    fontStyle:'normal',
    color:'#92FE9D'
   
},

colView:
{
    alignContent:'center',
    justifyContent:'center',
    paddingLeft:10,
    paddingRight:10,
    width:wp('50%')
}


	
})
