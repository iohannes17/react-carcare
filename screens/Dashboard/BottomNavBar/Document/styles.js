import {StyleSheet} from 'react-native';
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from 'react-native-responsive-screen'


export default StyleSheet.create({

	buttonView:{
		width:wp('40%'),
		height:hp('5%'),
		borderRadius: 10,
	},
	gradientStyle:{
		 alignItems: 'center',
		  borderRadius: 10,
		  width:wp('40%'),
		  height:hp('5%'),
		  justifyContent:'center',
		  alignContent:'center'
	},
	innerroundShape:{
        width: wp('70%'),
		borderRadius: 10,
		alignSelf:'center',
		alignSelf:'center',
		justifyContent:'center',
		alignContent:'center'
		},
		
		container: {
       
			backgroundColor:'#101112',
			alignItems: 'center',
			justifyContent: 'center',
		  },

		  iconText:{
			color:'#101112',
			fontSize: 10,
			fontFamily:'Lato-Bold',
			fontWeight:'900',
			textAlign:'center',
		}
		
})
