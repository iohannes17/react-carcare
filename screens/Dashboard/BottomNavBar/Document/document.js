
//  Copyright © 2019 CarCare. All rights reserved.
//

import React from "react"
import {Image, TouchableOpacity, StyleSheet, View,
	 Text,ImageBackground,Picker,ScrollView,Alert,AsyncStorage,TouchableHighlight } from "react-native"
import styles from './styles'
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from 'react-native-responsive-screen'
import {DatePicker} from 'native-base'
import {LinearGradient} from 'expo-linear-gradient';
import Icon from 'react-native-vector-icons/Entypo'
import Toast,{DURATION} from 'react-native-easy-toast';
import {connect} from 'react-redux';
import {getUserCars} from '../../../../Services/ApiActions/getCars';
import PropTypes from 'prop-types'
import * as Permissions from 'expo-permissions';
import { Notifications } from 'expo';








 class Documents extends React.Component {

	static navigationOptions = ({ navigation }) => {
	
		const { params = {} } = navigation.state
		return {
				headerTitle:'Document Reminder'
			}
	}
	
	constructor(props) {
		super(props);
		this.state = {
		  documentType:'Certificate of Road Worthiness',
		  chosenDate: new Date(), 	
		  selectedCar: 'non'
		};
		
		this.setDate = this.setDate.bind(this);
	  }

	  componentDidMount(){
		  this.props.getUserCars();
		  this.alertIfRemoteNotificationsDisabledAsync();
	  }


	  alertIfRemoteNotificationsDisabledAsync = async ()=> {
		const { status } = await Permissions.getAsync(Permissions.NOTIFICATIONS);
		if (status !== 'granted') {
		  Alert.alert('Hey! You might want to enable notifications for carcare.');
		}
		
	}


	scheduleNotification = async () => {
		  
		const document = this.state.documentType;
		const car = this.state.selectedCar;
		const when = this.state.chosenDate;

	    const days = 7 // Days you want to subtract
		const date = this.state.chosenDate;
		const last = new Date(date.getTime() - (days * 24 * 60 * 60 * 1000));
		// const day =last.getDate();
		// const month=last.getMonth()+1;
		// const year=last.getFullYear();
		

		let notificationId = Notifications.scheduleLocalNotificationAsync(
		  {
			title: "Your " + car ,
			body: document+ " will expire at " + when,
		  },
		  {
			// repeat: 'minute',
			time: when.getTime() - 25200000
		  },
		);
		
		//  AsyncStorage.setItem('notificationId',notificationId);
	  };


	  onDocumentSelected=(value)=> {
		this.setState({
		  documentType: value
		});
	  }

	  onCarValueChange=(value)=> {
		this.setState({
		  selectedCar: value
		});
	  }


	  setDate(newDate) {
		this.setState({ chosenDate: newDate });
	  }

      _onSubmit = async ()=>{


				if(this.state.selectedCar === "non"){
					Alert.alert('','No available vehicle linked to this user')
				}
				else{
            	  
					Alert.alert('','Reminder for ' + this.state.selectedCar + ' ' + this.state.documentType + ' reminder set for 7-hours earlier ');
					this.scheduleNotification();
	 
				}
	
   }
	 








	render() {



			let arr = this.props.usersCarList;
			
		return  <View Style={styles.container}>
		           
		<View style={{height:hp('2%'),width:wp('100%'),backgroundColor:'#101112'}}></View>
		  
		 <ImageBackground source={require('../../../../assets/images/bg.png')} style={{
						 width:wp('100%'), 
						 height:hp('28%'),
						 }}>


		<Text style={{textAlign:'left',fontFamily:'Poppins-Bold',fontSize:25,color:'#ffffff',left:5,position:'absolute'}}>
			{'\n'}Early reminder alert{'\n'} on your car document expiration</Text>
		

						
					</ImageBackground>      
					<View style={{alignItems:'center', backgroundColor:'#101112', height:hp('90%')}}>

							 <View style={{width:wp('100%'), height:hp('3%')}}></View>	

                            	<LinearGradient
							  colors={['#00C9FF','#92FE9D']}
							  start={[0.0, 0.9]}
							  end={[1.0, 0.4]}
							  locations={[0.0, 1.0]}
							  style={styles.innerroundShape}>

                            
                              {/* User selects vehicle for service */}

						  <Picker
						  mode="dropdown"
						  iosIcon={<Icon name="arrow-down" />}
						  style={{ 
							  width:wp('70%'),
							  alignSelf:'center',
							  borderColor:'#92FE9D',
							
							  borderRadius:10,
							  opacity:0.4}}
						  placeholder="Select Vehicle"
						  placeholderStyle={{ color: "green" }}
						  selectedValue={this.state.selectedCar}
						  onValueChange={this.onCarValueChange.bind(this)}>	

							{arr.length == 0 ? <Picker.Item label="Car not available" value="non"/> :  
								 arr.map((item) =>{ return(<Picker.Item  label={item.brand} value={item.brand} key={item.id}/>);})
							 }
				  </Picker>																	  
			     </LinearGradient>	

                   <View style={{width:wp('100%'), height:hp('3%')}}></View>	
				  		
							<LinearGradient
							  colors={['#00C9FF','#92FE9D']}
							  start={[0.0, 0.9]}
							  end={[1.0, 0.4]}
							  locations={[0.0, 1.0]}
							  style={styles.innerroundShape}>


						  <Picker
						  mode="dropdown"
						  iosIcon={<Icon name="arrow-down" />}
						  style={{ 
							  width:wp('70%'),
							  alignSelf:'center',
							  borderColor:'#92FE9D',
							  borderRadius:10,
							  opacity:0.4}}
						  placeholder="Select Document"
						  placeholderStyle={{ color: "green" }}
						  selectedValue={this.state.documentType}
						  onValueChange={this.onDocumentSelected.bind(this)}>
 
					  
                    <Picker.Item label="Certificate of Road Worthiness" value="Certificate of Road Worthiness"/>   
				   <Picker.Item label="Insurance Certificate" value="Insurance Certificate" />
                          <Picker.Item label="Driver's License" value="Driver's License" />
                          <Picker.Item label="Vehicle License" value="Vehicle License" />
				  </Picker>																	  
			     </LinearGradient>	
				
				

						   <View style={{width:wp('100%'), height:hp('2%')}}></View>	
                  

							<LinearGradient
							  colors={['#00C9FF','#92FE9D']}
							  start={[0.0, 0.9]}
							  end={[1.0, 0.4]}
							  locations={[0.0, 1.0]}
							  style={styles.innerroundShape}>

								<DatePicker
								defaultDate={new Date(2019, 10, 10)}
								minimumDate={new Date(2019, 10, 10)}
								maximumDate={new Date(2050, 12, 31)}
								locale={"en"}
								timeZoneOffsetInMinutes={undefined}
								modalTransparent={true}
								animationType={"fade"}
                                androidMode={"calendar"}
								placeHolderText="Expiry Date"
								textStyle={{ color: "#ffffff" }}
								placeHolderTextStyle={{ color: "#101112" }}
								onDateChange={this.setDate}
								disabled={false}
								/>
																					  
			                 </LinearGradient>	
				       
				 <View style={{width:wp('100%'), height:hp('2%')}}></View>	

				 	 <TouchableHighlight style={styles.buttonView} onPress={this._onSubmit.bind(this)}>
					<LinearGradient
							colors={['#00C9FF','#92FE9D']}
							start={[0.0, 0.9]}
							end={[1.0, 0.4]}
							locations={[0.0, 1.0]}
							style={styles.gradientStyle}>
								
							<Text style={styles.iconText}>Set Reminder</Text>
																	
							</LinearGradient>
                          </TouchableHighlight>	

						
					</View>  		
					<Toast ref="toast"
						style={{backgroundColor:'#000000'}}
						position="bottom"
						fadeInDuration={1050}
						fadeOutDuration={3000}
						opacity={0.8}
						textStyle={{color:'#00C9FF'}}
						/>	
						
                 </View>
				
	}

}


const mapStateToProps = state =>{
	return{
	  usersCarList:state.carList.usersCar,
	 	
	};
  }
  
  Documents.propTypes ={
	getUserCars:PropTypes.func.isRequired,
	usersCarList:PropTypes.any,
	

  }

  export default connect(mapStateToProps, {getUserCars})(Documents);









