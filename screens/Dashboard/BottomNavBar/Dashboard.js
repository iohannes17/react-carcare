//
//  Dashboard
//  Cryptonance
//
//  Created by Cryptonance.
//  Copyright © 2018 Cryptonance. All rights reserved.
//

import React from "react"
import { View, TouchableOpacity, Text,Image,ScrollView,ImageBackground,AsyncStorage,Platform} from "react-native"
import styles from './styles';
import DashContainer from '../../../components/DashboardContainer'
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {LinearGradient} from 'expo-linear-gradient';
import MaterialIntials from 'react-native-material-initials/native';
import * as Animatable from 'react-native-animatable'
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {logOutApp} from '../../../Services/ApiActions/logOut'
import { Ionicons } from '@expo/vector-icons';







class DashboardScreen extends React.Component {


	static navigationOptions = ({ navigation }) => {
	
		const { params = {} } = navigation.state
		return {
				header: null,
				headerLeft: null,
				headerRight: null,
			}
	}


	constructor(props) {
		super(props);
		this.state = {
		   name:'Not Available',
		  
		};
	}
   

	async componentDidMount(){

        this.setState({
			name:await AsyncStorage.getItem('user_name')=== null ? 'Not Available':await AsyncStorage.getItem('user_name'),
			
		})
	}


	onPressVinDecoder =()=>{
    this.props.navigation.navigate('decoder')
	}

	onPressDocument =()=>{
		this.props.navigation.navigate('document')
		
	}

	onPressSchedules =()=>{
		this.props.navigation.navigate('schedules')	
	}


	onPressReportCar =()=>{
		this.props.navigation.navigate('stolenCar')
	}



	logOut= async ()=>{
		this.props.logOutApp();
		AsyncStorage.removeItem('user_name');
		
	}
	 

	render() {

	  const name = this.state.name 
	
		return   <View Style={styles.dashboardView}>
		           
		            <View style={{height:hp('1%'),width:wp('100%'),backgroundColor:'#101112'}}></View>
		              
		             <ImageBackground source={require('../../../assets/images/bg.png')} style={{
									 width:wp('100%'), 
									 height:hp('28%'),
									 }}>
									   <Animatable.View animation="pulse" easing="ease-out" iterationCount="infinite" style={styles.roundShape}>
											<LinearGradient
														colors={['#00C9FF','#92FE9D']}
														start={[0.0, 0.9]}
														end={[1.0, 0.4]}
														locations={[0.0, 1.0]}
														style={styles.innerroundShape}>

														      <MaterialIntials
																	style={{alignSelf:'center',backgroundColor:'transparent'}}
																	color={'#000000'}
																	size={50}
																	text={name}
																	single={false}/> 
                                  
														
																</LinearGradient>		
											</Animatable.View>


											
								       
								</ImageBackground>      
								<View style={styles.bigView}>
								    <Animatable.View style={styles.rowView} animation="fadeInLeftBig" delay={500}>

											  <TouchableOpacity style={styles.buttonView} onPress={this.onPressSchedules}>
													<LinearGradient
														colors={['#00C9FF','#92FE9D']}
														start={[0.0, 0.9]} 
														end={[1.0, 0.4]}
														locations={[0.0, 1.0]}
														style={styles.gradientStyle}>
                                  <Image source={require('../../../assets/images/service.png')} style={styles.iconStyle}/>
																	 <Text style={styles.iconText}>VEHICLE SERVICES</Text>
																	 <Text style={styles.subiconText}>Track vehicle service history</Text>
														    
														
																</LinearGradient>
                          </TouchableOpacity>				
                              
                           <View style={{width:wp('5')}}></View>

													 <TouchableOpacity style={styles.buttonView} onPress={this.onPressVinDecoder}>
													<LinearGradient
														colors={['#00C9FF','#92FE9D']}
														start={[0.0, 0.9]}
														end={[1.0, 0.4]}
														locations={[0.0, 1.0]}
														style={styles.gradientStyle}>
														    <Image source={require('../../../assets/images/magnifier.png')} style={styles.iconStyle2}/>
																	 <Text style={styles.iconText}>VIN DECODER</Text>
																	 <Text style={styles.subiconText}>Decode vehicle information</Text>
														    
														
														
																</LinearGradient>
                          </TouchableOpacity>															
									  </Animatable.View>	

										<View style={{height:wp('5')}}></View>

										<Animatable.View style={styles.rowView} animation="fadeInRightBig" delay={500}>
										<TouchableOpacity style={styles.buttonView} onPress={this.onPressDocument}>
											<LinearGradient
												colors={['#00C9FF','#92FE9D']}
												start={[0.0, 0.9]}
												end={[1.0, 0.4]}
												locations={[0.0, 1.0]}
												style={styles.gradientStyle}>
															<Image source={require('../../../assets/images/document.png')} style={styles.iconStyle3}/>
															<Text style={styles.iconText}>VEHICLE DOCUMENT</Text>
															<Text style={styles.subiconText}> 2-week notification before document expiration</Text>
														
												
														</LinearGradient>
											</TouchableOpacity>				
													
											<View style={{width:wp('5')}}></View>

											<TouchableOpacity style={styles.buttonView} onPress={this.onPressReportCar}>
											<LinearGradient
												colors={['#00C9FF','#92FE9D']}
												start={[0.0, 0.9]}
												end={[1.0, 0.4]}
												locations={[0.0, 1.0]}
												style={styles.gradientStyle}>
														<Image source={require('../../../assets/images/vincoder.png')} style={styles.iconStyle4}/>
															<Text style={styles.iconText}>VEHICLE THEFT</Text>
															<Text style={styles.subiconText}>Immediately Report car theft to alert authorities</Text>							
														</LinearGradient>
											</TouchableOpacity>															
							</Animatable.View>	


							 <View style={{height:hp('1.5%')}}></View>

							                
                                            <TouchableOpacity
														 style={{alignItems:'center'}}
														 onPress={this.logOut} >
											<LinearGradient
														colors={['#00C9FF','#92FE9D']}
														start={[0.0, 0.9]}
														end={[1.0, 0.4]}
														locations={[0.0, 1.0]}
														style={[styles.innerroundShape]}>

														{Platform.OS === 'android' ? <Ionicons name="md-log-out" size={35} color="black"/>
														: <Ionicons name="ios-log-out" size={35} color="black"/>}

														
                                            </LinearGradient>		
											</TouchableOpacity>
										


								</View>  
							
	           </View>
	 }
	 
}



const mapStateToProps = state =>{
	return{
		
		
	};
  }
  
DashboardScreen.propTypes ={
	logOutApp:PropTypes.func.isRequired
  }

  export default connect(mapStateToProps, {logOutApp})(DashboardScreen);









