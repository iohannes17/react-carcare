//Car Care
//Add car Page
//  Copyright © 2018 Cryptonance. All rights reserved.
//

import { View, Image, TouchableOpacity, StyleSheet, Text,ImageBackground,ScrollView,KeyboardAvoidingView,Alert } from "react-native"
import Icon from 'react-native-vector-icons/Entypo'
import styles from './styles'
import React from "react"
import DashContainer from '../../../../components/DashboardContainer'
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {Form, Item, Input, Label } from 'native-base';
import {LinearGradient} from 'expo-linear-gradient';


export default class StolenCar extends React.Component {

	static navigationOptions = ({ navigation }) => {
	
		const { params = {} } = navigation.state
		return {
				headerTitle:'Report Car Theft'
			}
	}

	
	constructor(props) {
		super(props);
		this.state = {
		  selected2: undefined,
		  fullname:'',
		  vin:'',
		  model:'',
		  brand:'',
		  color:''
		};
	  }


	  onReport=()=> {
		
		if(this.state.fullname === ''||
		   this.state.vin === ''||
		   this.state.model === ''||
		   this.state.brand === ''||
		   this.state.color === ''){

             Alert.alert('','Please make sure all details are filled');
		   }

		   else if(this.state.vin.length < 17 || this.state.vin.length > 17){
			       Alert.alert('','Incorrect VIN/Chassis number');
		
		   }
		   
		   else{

			Alert.alert('','Vehicle data has been sent to undergo verification process');
			Alert.alert('','Vehicle will be flagged as stolen once its been confirmed..Thank you');
		   }
	
	  }
	

	render() {
	
		return  <View Style={styles.container}>
		           
		<View style={{height:hp('1%'),width:wp('100%'),backgroundColor:'#101112'}}></View>
		        
					<ScrollView contentContainerStyle={{alignItems:'center', backgroundColor:'#101112', }}>


					<KeyboardAvoidingView style={{alignContent:'center',alignItems:'center'}} behavior='padding'>


					  <ImageBackground source={require('../../../../assets/images/bg.png')} style={{
									 width:wp('100%'), 
									 height:hp('25'),
									 alignItems:'center',
									 justifyContent:'center',
									 padding:2
									 }}>

									 <Text style={{textAlign:'left',fontFamily:'Poppins-Bold',fontSize:25,color:'#ffffff',left:5,position:'absolute'}}>
				                     {'\n'}Track and recover {'\n'}your stolen vehicle with the might of Nigerian force</Text>
									 
								       
								</ImageBackground>   

					<View style={{width:wp('100%'), height:hp('2%')}}></View>	
                           
							<Item stackedLabel style={styles.itemStyle}>
							<Label style={{padding:10,color:'#92FE9D',fontSize:9,fontFamily:'Poppins-Bold'}}>Registered Name on Vehicle</Label>
							<Input style={{color:'#ffffff',paddingLeft:5}}
							    onChangeText={(value)=> this.setState({fullname:value})} value={this.state.fullname}/>
							</Item>

							  <View style={{width:wp('100%'), height:hp('2%')}}></View>	

						   <Item stackedLabel style={styles.itemStyle}>
							<Label style={{padding:10,color:'#92FE9D',fontSize:9,fontFamily:'Poppins-Bold'}}>VIN|Chassis or Plate Number</Label>
							<Input style={{color:'#ffffff',paddingLeft:5}}
							   onChangeText={(value)=> this.setState({vin:value})} value={this.state.vin}/>
							</Item>

							  <View style={{width:wp('100%'), height:hp('2%')}}></View>	

							<Item stackedLabel style={styles.itemStyle}>
							<Label style={{padding:10,color:'#92FE9D',fontSize:9,fontFamily:'Poppins-Bold'}}>Model</Label>
							<Input style={{color:'#ffffff',paddingLeft:5}}
							   onChangeText={(value)=> this.setState({model:value})} value={this.state.model}/>
							</Item>
							

							<View style={{width:wp('100%'), height:hp('3%')}}></View>	

                            	<Item stackedLabel style={styles.itemStyle}>
							<Label style={{padding:10,color:'#92FE9D',fontSize:9,fontFamily:'Poppins-Bold'}}>Brand</Label>
							<Input style={{color:'#ffffff',paddingLeft:5}}
							   onChangeText={(value)=> this.setState({brand:value})} value={this.state.brand}/>
							</Item>
							
					  		
							<View style={{width:wp('100%'), height:hp('3%')}}></View>	

                            	<Item stackedLabel style={styles.itemStyle}>
							<Label style={{padding:10,color:'#92FE9D',fontSize:9,fontFamily:'Poppins-Bold'}}>Color</Label>
							<Input style={{color:'#ffffff',paddingLeft:5}}
							   onChangeText={(value)=> this.setState({color:value})} value={this.state.color}/>
							</Item>
                        	
				 <View style={{width:wp('100%'), height:hp('3%')}}></View>	

				 	 <TouchableOpacity style={styles.buttonView} onPress={this.onReport}>
					<LinearGradient
							colors={['#00C9FF','#92FE9D']}
							start={[0.0, 0.9]}
							end={[1.0, 0.4]}
							locations={[0.0, 1.0]}
							style={styles.gradientStyle}>
								
							<Text style={styles.iconText}>Submit</Text>
																	
							</LinearGradient>
                          </TouchableOpacity>			
						
						  </KeyboardAvoidingView>
					</ScrollView>  
                 </View>
				
	}
}
