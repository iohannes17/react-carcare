
//
//  Created by john.
//  Copyright © 2019 Carcare. All rights reserved.
//

import { View,TouchableOpacity, Switch, StyleSheet, 
		Text,TouchableHighlight,ScrollView,KeyboardAvoidingView,
		AsyncStorage,
		Image,ImageBackground,ActivityIndicator} from "react-native"
import {Picker} from "native-base";
import Icon from 'react-native-vector-icons/Entypo'
import {LinearGradient} from 'expo-linear-gradient';
import {Item, Input, Label} from 'native-base';
import styles from './styles'
import React from "react"
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from 'react-native-responsive-screen'
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {CreateProfileApp} from '../../../../Services/ApiActions/Profile';
import Toast,{DURATION} from 'react-native-easy-toast'






 class Profile extends React.Component {

	static navigationOptions = ({ navigation }) => {
	
		const { params = {} } = navigation.state
		return {
				header: null,
				headerLeft: null,
				headerRight: null,
			}
	}

	
	constructor(props) {
		super(props);
		this.state = {
		 fullname:'',
		 email:'',
		 phone:'',
		 address:'',		  
		 city:'',
		 statee:''
		};
		
	  }

  
	  async componentDidMount(){

		this.setState({
			email:await AsyncStorage.getItem('user_email'),
			fullname: await AsyncStorage.getItem('fullname'),
			address: await AsyncStorage.getItem('address'),
			phone: await AsyncStorage.getItem('phone'),
			city: await AsyncStorage.getItem('city'),
			statee: await AsyncStorage.getItem('state')
	
		})
		
	  }

	


	  _createProfile = async ()=>{

		if(this.state.fullname === '' ||
		  this.state.email=== '' ||
		  this.state.city === '' ||
		  this.state.statee === ''||
		  this.state.phone === '' ){

			this.refs.toast.show('Plase fill all fields');
			
		  } 
		  

		  else{
			 
			await AsyncStorage.setItem('fullname',this.state.fullname);
			await AsyncStorage.setItem('phone',this.state.phone);
			await AsyncStorage.setItem('state',this.state.statee);
			await AsyncStorage.setItem('city',this.state.city);
			await AsyncStorage.setItem('address',this.state.address);
			await AsyncStorage.setItem('activated','activated');

			
			
			 const data ={
				   fullname:this.state.fullname,
				   address: this.state.address,
				   email:this.state.email,
				   phone:this.state.phone,
				   city:this.state.city,
				   state:this.state.statee,
				   deviceid: await AsyncStorage.getItem('tokenkey'),
				   
			 };

			
			 this.props.CreateProfileApp(data)
			
		  }

	  }


	render() {	
	
		return <View styl={styles.container}>
		                  
						
						  <ScrollView contentContainerStyle={{alignItems:'center', backgroundColor:'#101112', }}>
                               <KeyboardAvoidingView style={{alignContent:'center',alignItems:'center'}} behavior='padding'>


				       
					   <ImageBackground source={require('../../../../assets/images/bg.png')} style={{
									 width:wp('100%'), 
									 height:hp('32'),
									 alignItems:'center',
									 justifyContent:'center',
									 padding:3
									 }}>

									 <Text style={{textAlign:'left',fontFamily:'Poppins-Bold',fontSize:25,color:'#ffffff',left:5,position:'absolute'}}>
				                            {'\n'}Link your bio {'\n'}data to your vehicle for tracking and security</Text>
									 
								       
								</ImageBackground>   


				   
									<View style={{height:hp('10%'),width:wp('100%'),backgroundColor:'#101112'}}></View>

					<Item stackedLabel style={styles.itemStyle}>
					<Label style={{padding:10,color:'#92FE9D',fontSize:9,fontFamily:'Poppins-Bold'}}>Full Name</Label>
					<Input style={{color:'#ffffff',paddingLeft:10}} maxLength={100} 
							              onChangeText={(value)=> this.setState({fullname:value})} value={this.state.fullname}/>
					</Item>

					  
					
					<View style={{height:hp('1.5%'),width:wp('100%'),backgroundColor:'#101112'}}></View>

						<Item stackedLabel style={styles.itemStyle}>
						<Label style={{padding:10,color:'#92FE9D',fontSize:9,fontFamily:'Poppins-Bold'}}>Email</Label>
						<Input style={{color:'#ffffff',paddingLeft:10}} maxLength={100} 
							              onChangeText={(value)=> this.setState({email:value})} value={this.state.email}/>
							
						</Item>

					<View style={{height:hp('1.5%'),width:wp('100%'),backgroundColor:'#101112'}}></View>

					<Item stackedLabel style={styles.itemStyle}>
					<Label style={{padding:10,color:'#92FE9D',fontSize:9,fontFamily:'Poppins-Bold'}}>Phone Number</Label>
					<Input style={{color:'#ffffff',paddingLeft:10}} maxLength={13} 
							              onChangeText={(value)=> this.setState({phone:value})} value={this.state.phone}/>
							
					</Item>


					<View style={{height:hp('1.5%'),width:wp('100%'),backgroundColor:'#101112'}}></View>

					<Item stackedLabel style={styles.itemStyle}>
					<Label style={{padding:10,color:'#92FE9D',fontSize:9,fontFamily:'Poppins-Bold'}}>Address</Label>
					<Input style={{color:'#ffffff',paddingLeft:10}} maxLength={40} 
					  onChangeText={(value)=> this.setState({address:value})} value={this.state.address}/>
							
					</Item>
                    
					<View style={{height:hp('1.5%'),width:wp('100%'),backgroundColor:'#101112'}}></View>

					<Item stackedLabel style={styles.itemStyle}>
					<Label style={{padding:10,color:'#92FE9D',fontSize:9,fontFamily:'Poppins-Bold'}}>City</Label>
					<Input style={{color:'#ffffff',paddingLeft:10}} maxLength={40} 
					  onChangeText={(value)=> this.setState({city:value})} value={this.state.city}/>			
					</Item>

					<View style={{height:hp('1.5%'),width:wp('100%'),backgroundColor:'#101112'}}></View>

					<Item stackedLabel style={styles.itemStyle}>
					<Label style={{padding:10,color:'#92FE9D',fontSize:9,fontFamily:'Poppins-Bold'}}>State</Label>
					<Input style={{color:'#ffffff',paddingLeft:10}} maxLength={40} 
					onChangeText={(value)=> this.setState({statee:value})} value={this.state.statee}/>
							
					</Item>


				
					 
					<View style={{height:hp('1.5%'),width:wp('100%'),backgroundColor:'#101112'}}></View>

					 <TouchableOpacity style={styles.buttonView} onPress={this._createProfile}>

				<LinearGradient
				colors={['#00C9FF','#92FE9D']}
				start={[0.0, 0.9]}
				end={[1.0, 0.4]}
				locations={[0.0, 1.0]}
				style={styles.gradientStyle}>
					
				 {this.props.isLoading ?
				  <ActivityIndicator size={32} color="white"/> : 
				  	<Text style={styles.iconText}>Update</Text>}
														
				</LinearGradient>
			  </TouchableOpacity>	

			<View style={{height:hp('2%'),width:wp('100%'),backgroundColor:'#101112'}}></View>  
		</KeyboardAvoidingView> 
					
		</ScrollView>
		<Toast ref="toast"
		 style={{backgroundColor:'#000000'}}
		 position="bottom"
		 fadeInDuration={1050}
		 fadeOutDuration={3000}
		 opacity={0.8}
		 textStyle={{color:'#00C9FF'}}
		/>
		</View>
		  
		
	
	}
}




const mapStateToProps = state =>{
	return{
		
		isLoading: state.createProfile.isCreating
	};
  }
  
  Profile.propTypes ={
	CreateProfileApp:PropTypes.func.isRequired,
	isLoading:PropTypes.bool
  }

  export default connect(mapStateToProps, {CreateProfileApp})(Profile);







