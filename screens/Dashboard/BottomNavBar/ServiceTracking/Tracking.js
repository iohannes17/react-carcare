//Car Care
//view user car schedules
//  Copyright © 2018 Cryptonance. All rights reserved.
//

import { View, Image, TouchableOpacity, Switch, StyleSheet, Text,ImageBackground,
	ScrollView,Picker,KeyboardAvoidingView,ActivityIndicator} from "react-native"
import React from "react"
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from 'react-native-responsive-screen';
import CustomListview from './customList'
import {getUserSchedules} from '../../../../Services/ApiActions/getScheduledService'
import PropTypes from "prop-types";
import {connect} from 'react-redux';






 class CarSchedules extends React.Component {


  static navigationOptions = ({ navigation }) => {
	
		const { params = {} } = navigation.state
		return {
			headerTitle:'Car Schedules'
			}
	}
	
	componentDidMount(){
		this.props.getUserSchedules();
	}



	 
	render() {

		if(this.props.carHist.length == 0 || this.props.carHist === undefined){

			return(
				<ScrollView contentContainerStyle={styles.container}>
					 
								<ActivityIndicator size="large" color='green'/>
								<Text style={{
								textAlign:'center',
								fontFamily:'Poppins-Bold',
								fontSize:15,
								color:'#FFFFFF',
		
								}}>
								{'\n'}No Schedules Available Now</Text>
				
								
							
				</ScrollView>
				);
		
      	 }

	 else{

			
		let content = <CustomListview cars={this.props.carHist} />;
   
		if (this.props.carHistLoading) {
		  content = 	<View style={styles.container}>
						 <ActivityIndicator size="large" color='green'/>
						 
						 </View>;
						  }
	
		return (
		 
			<ScrollView contentContainerStyle={styles.container}>
		   
				{content}
		  </ScrollView>
		   
		);
		
	 }
		

		
   }
}
		
		const styles = StyleSheet.create({
			container: {
				flex: 1, 
				justifyContent:'center',
				height:hp('100%'),
				width:wp('100%'),
				backgroundColor: "#1a1c1f",
			},
			indicator:{
				flex: 1, 
				justifyContent:'center',
				alignItems:'center',
				alignContent:'center',
		
		
			},
		});
		
		
		
		
		const mapStateToProps = state =>{
				return{
						carHist: state.schedules.userSched,
						carHistLoading: state.schedules.isFetching
					
				};
			}
			
			CarSchedules.propTypes ={
				getUserSchedules: PropTypes.func.isRequired,
				carHist:PropTypes.any,
				carHistLoading:PropTypes.bool
			
			}
			
			
			export default connect(mapStateToProps, {getUserSchedules})(CarSchedules);