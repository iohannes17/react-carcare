import React, { Component } from "react";
import { StyleSheet, FlatList, Text, Image, View,Touch } from "react-native";
import PropTypes from "prop-types";
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen'
import * as Animatable from 'react-native-animatable'
import {LinearGradient} from 'expo-linear-gradient';



class CustomListview extends Component {

  _keyExtractor = item => item.id_Key;

  _renderItem = ({ item }) => {
    const {Car,id,date,description,status,autoshop} = item;


    return (
      <View style={styles.container}>
        <Animatable.View style={styles.cardContainerStyle} animation="zoomInLeft">
        <LinearGradient
        colors={['#00C9FF','#92FE9D']}
        start={[0.0, 0.9]} 
        end={[1.0, 0.4]}
        locations={[0.0, 1.0]}
        style={styles.gradientStyle}>

          	<Text style={styles.headerText}>{Car} 
            </Text>


            <Text style={styles.leftText}>Service Date :  {"  "}
            <Text style={styles.rightText}>{date}</Text></Text>


          	<Text style={styles.leftText}>Service Type : {"  "}
            <Text style={styles.rightText}>{description}</Text></Text>

            	<Text style={styles.leftText}>AutoShop : {"  "}
            <Text style={styles.rightText}>{autoshop}</Text></Text>

            	<Text style={styles.leftText}>AutoShop Location: {"  "}
            <Text style={styles.rightText}>{' '}</Text></Text>


           	<Text style={styles.leftText}>Status :  {"  "}
            <Text style={styles.rightText}>{status}</Text></Text>

      
                      
        
         </LinearGradient>
          </Animatable.View>
      </View>
    );
  };



  render() {
    return (
      <FlatList
        style={{ flex: 1, }}
        data={this.props.cars}
        keyExtractor={this._keyExtractor}
        renderItem={this._renderItem}
        showsVerticalScrollIndicator={false}
        showsHorizontalScrollIndicator={false}
      />
    );
  }
}

CustomListview.propTypes = {
  cars: PropTypes.array,
};




export default CustomListview;


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "transparent"
  },
  cardContainerStyle: {
    flex: 1,
    flexDirection: "row",
    elevation:2,
    padding:5
  },
  
  
  gradientStyle:{
   
     borderRadius: 20,
     width:wp('90%'),
     height:hp('25%'),
    paddingLeft:10,
    paddingTop:8
},

leftText:{
    textAlign:'center',
    fontFamily:'Poppins-Bold',
    fontSize:12,
    color:'#000000',
   
},


headerText:{
    marginTop:7,
    textAlign:'center',
    fontFamily:'Poppins-Bold',
    fontSize:15,
    color:'#000000',
   
},

rightText:{
    fontFamily:'Poppins-Regular',
    fontSize:10,
    textAlign:'center',
    color:'#000000',
  
    
}

});
