//
//  Onboarding2
//  Cryptonance
//
//  Created by Supernova.
//  Copyright © 2018 Supernova. All rights reserved.
//

import { View, Animated, Easing, TouchableOpacity, Image, Text, StyleSheet } from "react-native"
import React from "react"
import styles from './styles'


export default class Onboarding2 extends React.Component {

	static navigationOptions = ({ navigation }) => {
	
		const { params = {} } = navigation.state
		return {
				header: null,
				headerLeft: null,
				headerRight: null,
			}
	}

	constructor(props) {
		super(props)
		this.state = {
			exchangeImageScale: new Animated.Value(-1),
			exchangeImageOpacity: new Animated.Value(-1),
		}
	}

	componentDidMount() {
	
		this.startAnimationOne()
	}

	onSkipPressed = () => {
	
	}

	startAnimationOne() {
	
		// Set animation initial values to all animated properties
		this.state.exchangeImageScale.setValue(0)
		this.state.exchangeImageOpacity.setValue(0)
		
		// Configure animation and trigger
		Animated.parallel([Animated.parallel([Animated.timing(this.state.exchangeImageScale, {
			duration: 4000,
			easing: Easing.bezier(0.22, 0.61, 0.36, 1),
			toValue: 1,
		}), Animated.timing(this.state.exchangeImageOpacity, {
			duration: 1000,
			easing: Easing.bezier(0.22, 0.61, 0.36, 1),
			toValue: 1,
		})])]).start()
	}

	render() {
	
		return <View
				style={styles.onboardingView}>

                 <Animated.View
						style={[{
							opacity: this.state.exchangeImageOpacity.interpolate({
								inputRange: [-1, 0, 0.6, 1],
								outputRange: [1, 0, 1, 1],
							}),
							transform: [{
								scale: this.state.exchangeImageScale.interpolate({
									inputRange: [-1, 0, 0.2, 0.4, 0.6, 0.8, 1],
									outputRange: [1, 0.3, 1.1, 0.9, 1.03, 0.97, 1],
								}),
							}],
						}, styles.ImageAnimated]}>
							<Image
						source={require("../../../assets/images/Onboarding2.png")}
						style={styles.iconImage}/>
					</Animated.View>
			</View>
	}
}


