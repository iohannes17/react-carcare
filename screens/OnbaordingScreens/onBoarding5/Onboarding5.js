
import { View, Animated, Easing, Image, Text, StyleSheet } from "react-native"
import React from "react"
import styles from './styles'

export default class Onboarding5 extends React.Component {

	static navigationOptions = ({ navigation }) => {
	
		const { params = {} } = navigation.state
		return {
				header: null,
				headerLeft: null,
				headerRight: null,
			}
	}

	constructor(props) {
		super(props)
		this.state = {
			timeSaving1ImageScale: new Animated.Value(-1),
			timeSaving1ImageOpacity: new Animated.Value(-1),
		}
	}

	componentDidMount() {
	
		this.startAnimationOne()
	}

	startAnimationOne() {
	
		// Set animation initial values to all animated properties
		this.state.timeSaving1ImageScale.setValue(0)
		this.state.timeSaving1ImageOpacity.setValue(0)
		
		// Configure animation and trigger
		Animated.parallel([Animated.parallel([Animated.timing(this.state.timeSaving1ImageScale, {
			duration: 4000,
			easing: Easing.bezier(0.22, 0.61, 0.36, 1),
			toValue: 1,
		}), Animated.timing(this.state.timeSaving1ImageOpacity, {
			duration: 1000,
			easing: Easing.bezier(0.22, 0.61, 0.36, 1),
			toValue: 1,
		})])]).start()
	}

	render() {
	
		return <View
				style={styles.onboarding1View}>
					<Image
						source={require("../../../assets/images/Onboarding.png")}
						style={styles.iconImage}/>
			
			</View>
	}
}