//
//  Onboarding4
//  Cryptonance
//
//  Created by Cryptonance.
//  Copyright © 2018 Cryptonance. All rights reserved.
//

import { StyleSheet, TouchableOpacity, View, Image, Text,Animated, Easing, } from "react-native"
import React from "react"
import styles from './styles'


export default class Onboarding4 extends React.Component {

	static navigationOptions = ({ navigation }) => {
	
		const { params = {} } = navigation.state
		return {
				header: null,
				headerLeft: null,
				headerRight: null,
			}
	}

	constructor(props) {
		super(props)
		this.state = {
			creditCardsPaymentImageScale: new Animated.Value(-1),
			creditCardsPaymentImageOpacity: new Animated.Value(-1),
		}
	}

	componentDidMount() {
	
		this.startAnimationOne()
	}

	onSkipPressed = () => {
	
		const { navigate } = this.props.navigation
		
		navigate("LandingScreen")
	}


	startAnimationOne() {
	
		// Set animation initial values to all animated properties
		this.state.creditCardsPaymentImageScale.setValue(0)
		this.state.creditCardsPaymentImageOpacity.setValue(0)
		
		// Configure animation and trigger
		Animated.parallel([Animated.parallel([Animated.timing(this.state.creditCardsPaymentImageScale, {
			duration: 4000,
			easing: Easing.bezier(0.22, 0.61, 0.36, 1),
			toValue: 1,
		}), Animated.timing(this.state.creditCardsPaymentImageOpacity, {
			duration: 1000,
			easing: Easing.bezier(0.22, 0.61, 0.36, 1),
			toValue: 1,
		})])]).start()
	}

	render() {

					return <View
					style={styles.onboarding3View}>
				
								<Image
							source={require("../../../assets/images/Onboarding4.png")}
							style={styles.iconImage}/>
					
					</View>
			}
}	
			














