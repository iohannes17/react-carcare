import {StyleSheet} from 'react-native'
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from 'react-native-responsive-screen'




export default StyleSheet.create({
	onboarding3View: {
		backgroundColor: "rgb(22, 32, 42)",
		flex: 1,
		alignItems: "center",
	},
	iconImage: {
		backgroundColor: "transparent",
		resizeMode: "cover",
		width: wp('100%'),
		height: hp('100%'),
	},
})
