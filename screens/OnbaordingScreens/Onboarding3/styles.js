import {StyleSheet} from 'react-native'
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from 'react-native-responsive-screen'




export default StyleSheet.create({
	onboarding1View: {
		backgroundColor: "transparent",
		flex: 1,
		justifyContent: "center",
		alignItems: "center",
	},
	ImageAnimated: {
		width: 185,
		height: 185,
	},
	iconImage: {
		backgroundColor: "transparent",
		resizeMode: "cover",
		width: wp('100%'),
		height: hp('100%'),
	},
	
})