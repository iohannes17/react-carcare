/**
 * Onboarding page for the mobile app
 * explains the application features of the program
 * @author: Owodiye John
 */


import {StyleSheet, View,TouchableOpacity,Text} from 'react-native';
import React, {Component} from 'react';
import {IndicatorViewPager, PagerDotIndicator} from 'rn-viewpager';
import Onboarding1 from './Onboarding1/Onboarding1'
import Onboarding2 from './Onboarding2/Onboarding2'
import Onboarding3 from './Onboarding3/Onboarding3'
import Onboarding4 from './Onboarding4/Onboarding4'
import Onboarding5 from './onBoarding5/Onboarding5'
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {loginApp} from '../../Services/ApiActions/Login';






const style = StyleSheet.create({
  container:{
    flex: 1,
    height:'100%',
  },
  backgroundImage:{
    flex: 1,
    position:'absolute',
    width:'100%',
    height:'100%',
    alignItems:'center',
    justifyContent: 'center'
  },


  bottomView:{
    justifyContent:'flex-end',
    width:'100%',
    backgroundColor:'transparent',
    justifyContent:'center',
    position:'absolute',
    right:20,
    bottom:23,
  },


  skipButton:{
      backgroundColor:'transparent',
      width:'20%',
      alignSelf:'flex-end',
    
  },

  skipButtonText:{
    fontSize:13,
    alignSelf:'center',
    fontWeight:'normal',
    textAlign:'center',
    fontStyle:'normal',
    color:'#92FE9D',
    fontFamily:'Poppins-Bold'
},
      
});

 class OnBoardScreen extends Component {



    static navigationOptions = ({ navigation }) => {
	
		const { params = {} } = navigation.state
		return {
				header: null,
				headerLeft: null,
				headerRight: null,
			}
	}

        constructor(props) {
          super(props)
          }

  
          onSkipPressed = () => {
        
          this.props.navigation.navigate('Login');
        
        }



    render() {
        return (
            <View style={style.container}>
                <IndicatorViewPager
                    style={style.container}
                    indicator={this._renderDotIndicator()} >

                        <View>
                        <Onboarding5/>
                        </View>

                        <View>
                        <Onboarding1/>
                        </View>
                  
                        <View>
                        <Onboarding2/>
                        </View>

                        <View>
                        <Onboarding3/>
                        </View>

                        <View>
                        <Onboarding4/>
                        </View>  
                </IndicatorViewPager>

               <View style={style.bottomView}>
               <TouchableOpacity
						onPress={this.onSkipPressed}
						style={style.skipButton}>
						<Text
							style={style.skipButtonText}>Skip</Text>
					</TouchableOpacity>
               </View>
              
            </View>
        );
    }


    /**@this {_renderDotIndicator}
     * returns the number of dots for the pages in the onboard view
     */

    _renderDotIndicator() {
        return <PagerDotIndicator pageCount={5} selectedDotStyle={{backgroundColor:'#92fe9d'}} />;
    }

    
}


const mapStateToProps = state =>{
	return{
		
		
	};
  }
  
  OnBoardScreen.propTypes ={
	loginApp:PropTypes.func.isRequired
  }

  export default connect(mapStateToProps, {loginApp})(OnBoardScreen);
