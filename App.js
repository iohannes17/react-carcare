//
//  App.js
//
//  Created by Supernova.
//  Copyright © 2018 Supernova. All rights reserved.
//
 
import * as Font from 'expo-font'
import * as Permissions from 'expo-permissions'
import {AppLoading} from 'expo'
import React from "react"
import RootApp from './RootApp'
import {Component} from 'react'

import {createStore, applyMiddleware} from 'redux';
import {Provider} from 'react-redux';
import thunk from 'redux-thunk';
import rootReducers from './Services/rootReducer';
import { Platform, StatusBar,StatusBarIOS,AsyncStorage,Alert } from 'react-native';
import {Notifications} from 'expo';
import Constants from 'expo-constants'







const createStoreWithMiddleware = applyMiddleware(thunk)(createStore);
const store = createStoreWithMiddleware(rootReducers);





export default class App extends Component {

	constructor(props) {
		super(props)
		this.state = {
			fontsReady: false,
			notification: {},
		}
	}

	
	

	componentDidMount() {
	
    this.initProjectFonts()
    
     
    if(AsyncStorage.getItem('pushKey') === 'available'){
      
    }
    else{
      this.registerForPushNotificationsAsync();

    }
	

		 // Handle notifications that are received or selected while the app
    // is open. If the app was closed and then opened by tapping the
    // notification (rather than just tapping the app icon to open it),
    // this function will fire on the next tick after the app starts
    // with the notification data.
	this._notificationSubscription = Notifications.addListener(this._handleNotification);
	 this._handleNotification
	}


	_handleNotification = (notification) => {
		this.setState({notification: notification});
	  };




	  _handleNotification = ()=>{
		if(this.state.notification.origin === 'received'  || this.state.notification.origin === 'selected'){
			Alert.alert(
				'CarCare Notification' +' '+ this.state.notification.origin,
				this.state.notification.data,
				[{text:'OK'}],
			);
		}
	  };





	registerForPushNotificationsAsync = async () => {
		const { status: existingStatus } = await Permissions.getAsync(Permissions.NOTIFICATIONS);
		let finalStatus = existingStatus;
	
			// only ask if permissions have not already been determined, because
			// iOS won't necessarily prompt the user a second time.
			if (existingStatus !== 'granted') {
			// Android remote notification permissions are granted during the app
			// install, so this will only ask on iOS
			const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
			finalStatus = status;
			}
	
			// Stop here if the user did not grant permissions
			if (finalStatus !== 'granted') {
			return;
			}


			const deviceId = Constants.deviceId;
			try{
				
				let token = await Notifications.getExpoPushTokenAsync();
				AsyncStorage.setItem('tokenkey',token);
				AsyncStorage.setItem('pushKey','available');
		
			
			}catch(error){
			   
				Alert.alert('','Network Error..Please try again')

			}
	
			// Get the token that uniquely identifies this device
		
			// POST the token to your backend server from where you can retrieve it to send push notifications.
			}
	
	
	
	
	async initProjectFonts() {
	
		await Font.loadAsync({
			"Lato-Bold": require("./assets/fonts/LatoBold.ttf"),
			".SFNSText": require("./assets/fonts/SFNSText.ttf"),
			"Lato-Black": require("./assets/fonts/LatoBlack.ttf"),
			"Lato-Regular": require("./assets/fonts/LatoRegular.ttf"),
			"Poppins-Bold": require("./assets/fonts/Poppins-Bold.ttf"),
			"Poppins-Regular": require("./assets/fonts/Poppins-Regular.ttf"),
		})
		this.setState({
			fontsReady: true,
		})
	
	}

	

	render() {
	
		if (!this.state.fontsReady)
		 { 
			 return (<AppLoading/>); 
    }
    else{
		
		return <Provider store={store}>
			<RootApp/> 
			</Provider>;
      }
		
		
		   
	}
}