//
//  App.js
//  CarCare
//
//  Created by CarCare
//  Copyright © 2018 Supernova. All rights reserved.
//

import { Font, DangerZone,Constants } from "expo"
import React from "react"
import {createRootNavigator} from './navigation/AppNavigator';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {loginApp} from './Services/ApiActions/Login';
import { Platform, StatusBar,StatusBarIOS,AsyncStorage,View} from 'react-native'
import {Root} from 'native-base';
// import * as Permissions from 'expo-permissions';








 class RootApp extends React.Component {

	constructor(props) {
		super(props);
		
	}

	

	render() {
	
		const AppNavigator = createRootNavigator(this.props.isLoggedIn);

		return (
		
         
		<AppNavigator/>);
	}
}


const mapStateToProps = state =>{
	return{
		isLoggedIn: state.appLogin.isLoggedIn,
		
	};
  }
  
  RootApp.propTypes ={
	isLoggedIn:PropTypes.bool.isRequired,
  }

  export default connect(mapStateToProps, {})(RootApp);



  